import java.net.*;
import java.io.*;
import java.util.*;
import javax.crypto.*;
import java.security.*;
import org.ini4j.Ini;
import java.math.BigInteger;
import java.util.Arrays;
import java.lang.*;

@SuppressWarnings("unchecked")
public class Client{

    private final int maxHandlers = 10;
    private final int maxWaitingConn = 15;
    private Server server;
    private Socket sockTracker;
    private Socket sockPeer;
    private BufferedReader br;
    private PrintWriter pw;
    private BufferedInputStream bis;
    private BufferedOutputStream bos;
    private BufferedInputStream bisp;
    private BufferedOutputStream bosp;
    private String addressIP;
    private int id;
    private Hashtable<String, BitSet> seed;
    //enlever le private pour TestComPeer
    private Hashtable<String, BitSet> leech;
    private Hashtable<String, String> filenames;
    private int peerServerPort;
    private int i = 0;
    int pieceLen;
    private int maxPiece;



    public Client(String adressIP, int id) {
	try {
	    //récupération de données dans config.ini
	    Ini iniTracker = new Ini(new java.io.File("config.ini"));
	    this.pieceLen = iniTracker.get("peer-info","piece-length", int.class);
	    this.maxPiece = iniTracker.get("peer-info","max-piece", int.class);

	    this.br = new BufferedReader(new InputStreamReader(System.in));
	    this.pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);
	    //choix du port de connexion manuel
	    pw.println("Enter the server port of the peer");
	    peerServerPort = Integer.parseInt(br.readLine());
	    this.server = new Server(adressIP,peerServerPort,this.maxHandlers, this.maxWaitingConn, this);

	    this.addressIP = addressIP;
	    this.id = id;
	    this.seed = new Hashtable();
	    this.leech = new Hashtable();
	    this.filenames = new Hashtable();
	    //initialisation de la buffermap locale
	    initBuffermap();

	    //connexion au tracker
	    String adressTracker = iniTracker.get("tracker-info","tracker-address");
	    int portTracker = iniTracker.get("tracker-info","tracker-port", int.class);
	    sockTracker = new Socket(adressTracker, portTracker);

	    //pour test faux tracker
	    //InetAddress serv = InetAddress.getByName(Tracker.addressIP);
	    //sockTracker = new Socket(serv, 20000);
	    
	    bis = new BufferedInputStream(sockTracker.getInputStream());
	    bos = new BufferedOutputStream(sockTracker.getOutputStream());

	    server.start();
	    //automatisation de la fonction announce
	    announce();

	} catch (Exception ex) {
	    System.out.println("Initialisation");
	}
    }

    
    //envoi et réception fonctionnalité annonce
    public void announce() throws Exception{
	String messageToSend = "announce listen "+ this.peerServerPort + " seed [";
	//parcours de la table de seed pour obtenir les infos a transmettre
	Iterator<String> tmp = this.seed.keySet().iterator();
	String iterator;
	String sd = "";
	while(tmp.hasNext()){
	    iterator = tmp.next();
	    if (filenameFromKey(iterator) != "null") {
		try {
		    FileManager iterBufMap = new FileManager(this.id,filenameFromKey(iterator),this.pieceLen);
		    sd = sd + filenameFromKey(iterator) + " " + iterBufMap.getFileLength() + " " + iterBufMap.getPieceLength() + " " + iterator + " ";
		} catch (NoSuchAlgorithmException nsae) {
		    throw new IllegalStateException(nsae);
		}
	    }
	}
	sd = sd.trim();
	messageToSend = messageToSend + sd + "] leech [";
	//parcours de la table de seed pour obtenir les infos a transmettre
	Iterator<String> tmp2 = this.leech.keySet().iterator();
	String iterator2;
	String lc = new String();
	while(tmp2.hasNext()){
	    iterator2 = tmp2.next();
	    if (filenameFromKey(iterator2) != "null") {
		try {
		    FileManager iterBufMap = new FileManager(this.id,filenameFromKey(iterator2),this.pieceLen);
		    lc = lc + iterator2 + " ";
		} catch (NoSuchAlgorithmException nsae) {
		    throw new IllegalStateException(nsae);
		}
	    }
	}
	lc = lc.trim();
	messageToSend = messageToSend + lc + "]\r\n";

	messToSendTracker(messageToSend);
	String res = messRecFromTracker();
	pw.println("> " + res);
    }

    //fonction transformation et envoi octets au tracker
    public void messToSendTracker(String messageToSend) {
	try {
	    byte[] mts = messageToSend.getBytes();
	    bos.write(mts,0,mts.length);
	    bos.flush();
	    pw.println("< " + messageToSend);
	} catch (IOException e) {
	    System.out.println("Problème envoi message tracker");
	}
    }

    //fonction réception et transformation octets au tracker
    public String messRecFromTracker() {
	String res = new String();
	try {
	    StringBuffer sb = new StringBuffer();
	    int b;
	    while ((b=bis.read()) == -1) {}
	    sb.append((char)b);
	    while ((b=bis.read()) != -1) {
		if(b == '\n') break;
		sb.append((char)b);
	    }
	    res = sb.toString();
	} catch (IOException e) {
	    System.out.println("Problème réception message");
	}
	return res;
    }

    //stockage dans table hash des données renvoyées par fonctionnalité list
    public Hashtable<String, Integer> look(String m) {
	Hashtable<String, Integer> keyLen = new Hashtable<String, Integer>();
	String messageToSend = m;
	messToSendTracker(messageToSend);
	String res = messRecFromTracker();
	Requete req = new Requete();
	if (req.integrity(res) != false) {
	    for (int i = 0; i < req.indexes.length/4; i++) {
		try {
		    //création fichier local pour réceptionner les pieces ultérieurement
		    LocalFile file = new LocalFile(this.id, req.indexes[i*4], Integer.parseInt(req.indexes[i*4+2]));
		    file.setKey(req.indexes[i*4+3]);
		    file.setLength(Integer.parseInt(req.indexes[i*4+1]));
		    //leech a mtn connaissance d'un nouveau fichier avec sa clé
		    this.leech.put(file.getKey(), new BitSet());
		    this.filenames.put(file.getKey(), req.indexes[i*4]);
		    keyLen.put(file.getKey(), Integer.parseInt(req.indexes[i*4+1]));
		} catch (NoSuchAlgorithmException nsae) {
		    throw new IllegalStateException(nsae);
		}
	    }
	}
	pw.println("> " + res);
	return keyLen;
    }

    //fonction similaire a announce
    public void update() throws Exception{
	String messageToSend = "update seed [";
	Iterator<String> tmp = this.seed.keySet().iterator();
	String iterator;
	//FileManager iterBufMap;
	String sd = "";
	String infos = "####################\n";
	Object[] Buffer;
	while(tmp.hasNext()){
	    iterator = tmp.next();
		sd = sd + iterator + " ";
		infos += filenameFromKey(iterator) +" : 100%\n";

	}
	sd = sd.trim();
	messageToSend = messageToSend + sd + "] leech [";
	tmp = this.leech.keySet().iterator();
	String lc = new String();
	while(tmp.hasNext()){
	    iterator = tmp.next();
		lc = lc + iterator + " ";
		BitSet bslocal = leech.get(iterator);
		infos += filenameFromKey(iterator) +" : " + bslocal.cardinality() + " pieces\n";

	}
	infos += "####################";
	System.out.println(infos); //affiche sur le terminal la progression des téléchargements
	lc = lc.trim();
	messageToSend = messageToSend + lc + "]\r\n";

	messToSendTracker(messageToSend);
	String res = messRecFromTracker();
	pw.println("> " + res);
    }

    //gestion fermeture socket avec tracker
    public void endConTrack() {
	try {
	    bis.close();
	    bos.close();
	    sockTracker.close();
	} catch (IOException e) {
	    pw.println("Erreur endConTrack");
	}
    }


    //stockage données de connexion aux pairs possédant le fichier recherché
    public Hashtable<Integer,String> getfile(String key) {
	Hashtable<Integer,String> h = new Hashtable<Integer, String>();
	String messageToSend = "getfile " + key + " \r\n";
	messToSendTracker(messageToSend);
	String res = messRecFromTracker();
	pw.println("> " + res);

	Requete req = new Requete();
	if (req.integrity(res) != false) {
	    h = req.peers;
	    return h;
	}
	return null;
    }


    //connexion au pair dont on transmet les infos de connexion
    public void connectPeer(String adressIP, int port) {
	try {
	    InetAddress ip = InetAddress.getByName(adressIP);
	    sockPeer = new Socket(ip,port/*,InetAddress.getByName(this.adressIP),this.peerServerPort*/);
	    bisp = new BufferedInputStream(sockPeer.getInputStream());
	    bosp = new BufferedOutputStream(sockPeer.getOutputStream());
	    System.out.println("Connected to IP " + adressIP + " port " + port);
	} catch (UnknownHostException uhe) {
	    pw.println("Erreur connectPeer uhe");
	} catch (IOException e) {
	    pw.println("Erreur connectPeer e");
	}
    }

    //fonction transformation et envoi octets au peer
    public void messToSendPeer(String messageToSend) {
	try {
	    byte[] mts = messageToSend.getBytes();
	    Thread.sleep(2000);
	    bosp.write(mts,0,mts.length);
	    bosp.flush();
	    pw.println("< " + messageToSend);
	} catch (Exception e) {
	    System.out.println("Problème envoi message peer");
	}
    }

    //fonction réception et transformation octets au peer
    public String messRecFromPeer() {
	String res = new String();
	try {
	    StringBuffer sb = new StringBuffer();
	    int b;
	    while ((b=bisp.read()) == -1) {}
	    sb.append((char)b);
	    while ((b=bisp.read()) != -1) {
		if(b == '\n') break;
		sb.append((char)b);
	    }
	    res = sb.toString();
	} catch (IOException e) {
	    System.out.println("Problème réception message peer");
	}
	return res;
    }


    //débute téléchargement auprès des pairs qui possèdent le fichier recherché
    public Hashtable<String,BitSet> interested(Hashtable<Integer,String> h, String key, int length) throws Exception{
	int i = 0;
	if (h != null) {
	    //table stockant les @IP et buffermap des pairs auquel on se connecte
	    //uniquement utile quand @IP differentes et vérification d'infos
	    Hashtable<String,BitSet> hp = new Hashtable<String, BitSet>();
	    FileManager fm = new FileManager(this.id, filenameFromKey(key), this.pieceLen);
	    long len = fm.getFileLength();

	    //ne termine pas tant que fichier non telecharge entierement
	    while ( len < length) {
		Iterator<Integer> tmp = h.keySet().iterator();
		Integer iterator;
		while (tmp.hasNext() && len < length) {
		    iterator = tmp.next();
		    //connexion au pair
		    connectPeer(h.get(iterator), iterator);
		    String messageToSend = "interested " + key + "\r\n";
		    messToSendPeer(messageToSend);
		    String res = messRecFromPeer();
		    pw.println("> " + res);
		    
		    Requete req = new Requete();
		    req.integrity(res);
		    hp.put(h.get(iterator), req.bufferMap);
		    //appel automatique a getpieces et stockage du nombre de pieces
		    //telechargées pour infos
		    i += getpieces(req);
		    
		    //réactualisation pour connaitre les nouvelles données du fichier après
		    //chaque téléchargement partiel
		    fm = new FileManager(this.id, filenameFromKey(key), this.pieceLen);
		    len = fm.getFileLength();
		    /*Object[] Buffer = buffermapFromKey(key,this.pieceLen);
		      len = ((BitSet)Buffer[0]).cardinality() * this.pieceLen;
		    */
		}
	    }
	    //quand fichier entièrement téléchargé suppression dans leech et ajout dans seed
	    BitSet bt = leech.get(key);
	    this.seed.put(key,bt);
	    this.leech.remove(key);
	    return hp;
	}
	return null;
    }

    //demande de pieces au pair et stocke celles recues localement
    public int getpieces(Requete req) {
	String messageToSend = "getpieces " + req.fileKey + " [";
	//comparaison buffermap locale et celle du pair serveur connecté
	//pour ne pas télécharger les mêmes pièces
	BitSet bslocal = leech.get(req.fileKey);
	System.out.println("Buffermap locale : " + bslocal);
	BitSet bsreq = req.bufferMap;
	System.out.println("Buffermap pair connecté : " + bsreq);
	bsreq.andNot(bslocal); //index de tous les bits presents dans buffreq mais pas dans leech
	System.out.println("Pièces possibles à télécharger : " + bsreq);
	//choix des pieces a demander en nombre limité defini dans fichier config
	String pieceToAsk = "";
	int nbPTA = 0;
	for (int i = 0; i < bsreq.length(); i++) {
	    if (bsreq.get(i) && nbPTA < this.maxPiece) {
		pieceToAsk = pieceToAsk + i + " ";
		nbPTA += 1;
	    }
	}
	pieceToAsk = pieceToAsk.trim();
	messageToSend = messageToSend + pieceToAsk + "]\n";

	messToSendPeer(messageToSend);
	try {
	    //redéfinition réception mess pour cause de format de fonction have
	    String res = new String();
	    StringBuffer sb = new StringBuffer();
	    int b;
	    while ((b=bisp.read()) == -1) {}
	    sb.append((char)b);
	    while ((b=bisp.read()) != -1) {
		sb.append((char)b);
		if (b == ']') {
		    if((b=bisp.read()) == '\n') break;
		}
	    }
	    res = sb.toString();
	    //choix d'affichage long ou synthétique
	    pw.println("> have %pieces%");
	    //pw.println("> " + res);
	    Requete req2 = new Requete();
	    req2.integrity(res);

	    //actualisation des données locales du fichier pour chaque piece réceptionnées
	    String filename = filenameFromKey(req2.fileKey);
	    FileManager file = new FileManager(this.id, filename, this.pieceLen);
	    Iterator<Integer> tmp = req2.data.keySet().iterator();
	    int iterator;
	    while(tmp.hasNext()){
		iterator = tmp.next();
		file.addPiece(iterator, req2.data.get(iterator));
		leech.get(req.fileKey).set(iterator);
	    }
	    //fin telechargement fermeture socket
		messToSendPeer("close");
	    bisp.close();
	    bosp.close();
	    sockPeer.close();
	} catch (IOException e) {
	    System.out.println("Pb data");
	} catch (NoSuchAlgorithmException nsae) {
	    throw new IllegalStateException(nsae);
	}
	return nbPTA;
    }


    //fonctionnalité have similaire au retour de interested
    public void have(String key) {
	String messageToSend = "have " + key + " ";
	//Recupère la bufferMap du pair au quel le handler est associé
	Object[] bufferMap = this.buffermapFromKey(key,this.pieceLen);
	for (int i = 0; i < (int)bufferMap[1]; i++) {
	    if (((BitSet)bufferMap[0]).get(i))
		messageToSend += "1";
	    else
		messageToSend += "0";
	}
	messToSendPeer(messageToSend);
	String res = messRecFromPeer();
	pw.println("> " + res);
    }

    //Construction table de hachage
    public void initBuffermap(){
	try{
	    String path = "./src/peer/peersfiles/peer" + this.id + "/files/pieces/" ;
	    String[] files;
	    File directory = new File(path);
	    files = directory.list();
	    for (String filename : files){
		FileManager fm = new FileManager(this.id, filename, this.pieceLen);
		    this.seed.put(fm.getKey(), fm.getBufferMapFile());
		    this.filenames.put(fm.getKey(),filename);

	    }
	} catch(NoSuchAlgorithmException e) {
	}
    }

    public int getId(){
	return this.id;
    }

    public String filenameFromKey(String reqKey) {
      return this.filenames.get(reqKey);
    }

    public byte[] pieceFromKey(String reqKey, int i, int pieceLength) {
	byte[] b = new byte[this.pieceLen];
	try {
	    String filename = filenameFromKey(reqKey);
	    FileManager wantedFile = new FileManager(this.id, filename, pieceLength);
	    byte[] wantedPiece = wantedFile.getPiece(i);
	    return wantedPiece;
	} catch (NoSuchAlgorithmException nsae) {
	    throw new IllegalStateException(nsae);
	} catch (IOException e) {
	    pw.println("Erreur pieceFromKey");
	}
	return b;
    }

    public Object[] buffermapFromKey(String reqKey, int pieceLength){
	try {
	    String filename = filenameFromKey(reqKey);
	    FileManager file = new FileManager(this.id, filename, pieceLength);
	    BitSet bfm = file.getBufferMapFile();
	    int bfmSize = file.getBufferMapSize();
	    return new Object[]{bfm, bfmSize};
	} catch (NoSuchAlgorithmException nsae) {
	    pw.println("Erreur buffermapFromKey");
	    throw new IllegalStateException(nsae);
	}
    }

    public Hashtable<String, BitSet> getSeedMap(){
	return this.seed;
    }

    public Hashtable<String, BitSet> getLeechMap(){
	return this.leech;
    }

    public BitSet bitsetFromKey(String value){
	return this.seed.get(value);//La fonction get retourne la valeur associée à la la valeur value.
    }

    
    class Updater extends TimerTask{
	@Override
	public void run() {
	    try {
		Client.this.update();
	    } catch (Exception e) {
		System.out.println("Timer pb");
	    }
	}
    }
}


