import java.net.*;
import java.io.*;
import java.util.*;
import javax.crypto.*;
import java.security.*;
import java.math.BigInteger;


public class LocalFile{

  private final File file;
  private final File  piecesDirectory;
  private static final String piecesLocation = "files/pieces/";
  private String key;


  private final long fileSize;
  int pieceLength;
    int length;

  public LocalFile(int peerId, String filename, int pieceLength) throws NoSuchAlgorithmException{
    piecesDirectory = new File("./src/peer/peersfiles/peer" + peerId + "/" + piecesLocation + filename);
    piecesDirectory.mkdirs();
    file = new File(piecesDirectory.getParent() + "/../" + filename);
    this.fileSize = file.length();
    this.pieceLength = pieceLength;
    this.key = new String();
  }


  public void addPiece(byte[] piece, int pieceId) throws IOException, FileNotFoundException{
    FileOutputStream filePiece;
    String newPieceFileName = file.getParent() + "/pieces/" + file.getName() + "/" + pieceId;
    filePiece = new FileOutputStream(new File(newPieceFileName));
    filePiece.write(piece);
    filePiece.flush();
    filePiece.close();
  }

  public String getKey(){
    return this.key;
  }
  public void setKey(String key){
    this.key = key;
  }
    public void setLength(int length){
	this.length = length;
    }

    public int getLength(){
	return this.length;
    }


  public void sliceFile() throws IOException, FileNotFoundException{
    int i,j;
    int numberPiece = (int) (this.fileSize / this.pieceLength) + 1;
    FileOutputStream filePiece;
    String newPieceFileName;

    for (i = 0; i < numberPiece; i++){
      InputStream fileReader = new FileInputStream(this.file);
      int byteRegistered = 0;
      if (byteRegistered < file.length()){
        fileReader.skip(i*pieceLength);
        byte[] byteArray = new byte[pieceLength];
        byte[] lastPiece = new byte[fileReader.available()];
        if (i == numberPiece - 1){ //Si on arrive à la dernière pièce, on récupère byte par byte pour ne pas remplir le fichier de caractères null.
          lastPiece = new byte[fileReader.available()];
          fileReader.read(lastPiece);
        }
        else {
            fileReader.read(byteArray, 0, pieceLength);
        }

        newPieceFileName = file.getParent() + "/pieces/" + file.getName() + "/" + i;
        filePiece = new FileOutputStream(new File(newPieceFileName));

        if (i == numberPiece - 1){
          filePiece.write(lastPiece);
          filePiece.flush();
          filePiece.close();
          filePiece = null;
        }
        else{
          filePiece.write(byteArray);
          filePiece.flush();
          filePiece.close();
          filePiece = null;
        }
        byteRegistered+=pieceLength;
      }
    }
    file.delete();
  }


  public byte[] getPiece(int i) throws IOException{
    String newPieceFileName = file.getParent() + "/pieces/" + file.getName() + "/" + i;
    File filePiece = new File(newPieceFileName);
    FileInputStream pieceReader = new FileInputStream(filePiece);
    byte[] byteArray = new byte[pieceLength];
    pieceReader.read(byteArray, 0, pieceLength);
    return byteArray;
  }

   public static void main(String args[])throws NoSuchAlgorithmException, IOException, FileNotFoundException{
     LocalFile lf = new LocalFile(1001, "Dali.txt", 1024);
     lf.sliceFile();
   }
}
