uld go on to become one of the most famous paintings ever produced—Munch’s mental state was on full display, and his style varied greatly, depending on which emotion had taken hold of him at the time. The collection was a huge success, and Munch soon became known to the art world. Subsequently, he found brief happiness in a life otherwise colored by excessive drinking, family misfortune and mental distress.
Edvard Munch Self Portrait photo

Edvard Munch Selvportrett (Self-Portrait) 1881-82. Munch Museum, Oslo, Norway
Later Years and Legacy

Success wasn't enough to tame Munch's inner demons for long, however, and as the 1900s began, his drinking spun out of control. In 1908, hearing voices and suffering from paralysis on one side, he collapsed and soon checked himself into a private sanitarium, where he drank less and regained some mental composure. In the spring of 1909, he checked out, eager to get back to work, but as history would show, most of his great works were behind him.

Munch moved to a countr