/*
	*Un handler un thread qui recoit les communication entrantes
	sur le server et repond aux requetes.

	*Il est initialisé avec en paramètre la ra reference du pair pour lequel
	 il travaille. Cela permet de fournir des information sur ce que possède ce pair
	 et fournir les données en téléchargment


 */
import java.net.*;
import java.io.*;
import java.lang.String;
import java.util.BitSet;

public class Handler extends Thread{
    private int status;            // 1 si le handler est libre, 0 sinon
    private Socket socket;        //Socket de communication avec un pair
    private BufferedInputStream input; //buffer de reception
    private BufferedOutputStream output; //buffer d'envoie
	private Requete requete;      //requete recu
	private Client linkedPeer; //Pair auquel le handler est rattaché

    //Constructeur
    public Handler(Client p) {
	this.status = 1;
	this.linkedPeer = p;

    }

    //Lancement du travail d'un handler en lui donnant la nouvelle socket
    //de communication qu'il doit traiter.
    public void dealWith(Socket socket) {
		this.status = 0;
		this.requete = new Requete();
    	this.socket = new Socket();
		this.socket = socket;

		//recuperation des buffer de reception et d'envoie associé à la nouvelle socket
		try {
			this.input = new BufferedInputStream(this.socket.getInputStream());
			this.output = new BufferedOutputStream(this.socket.getOutputStream());
			this.start(); //Excecution de la méthode run
		} catch (Exception e) {
			System.out.println("Dans Handler dealWith : La communication ne peut être triatée");
		}
	}

    //Renvoie true si le handler est libre
    public boolean isfree() {
		return this.status == 1;
	}

	public void setbusy(){
    	this.status = 0;
	}

	public void closeConnexion() {
    	try {
			this.input.close();
			this.output.close();
			this.socket.close();
			this.status = 0;
		}
    	catch (Exception e){

		}
	}

    //Fonction d'exécution principale du handler
    //lit les requetes sur le buffer de reception et repond sur la sortie
    public void run() {
		int oneChar;
		byte[] bytesToSend;
		while (true) {
			try {
				String message = new String();
				String response = new String();

				//Lecture sur le buffer tant que le caractere de fin de requete n'est pas rencontré
				while ((oneChar = input.read()) != -1 && (char) oneChar != '\n') {
					message += (char) oneChar;
				}
				//Si le client coupe brusquement la connexion
				if (message.length() == 0) {
					closeConnexion();
					return;
				}

				//Affichage du message recu
				System.out.println("> " + /*"From " + this.socket.getPort() + " : " + */message);

				//Initialise et verifie la validité de la requête
				this.requete.integrity(message);


				//On switch le keyword et associe le traitement approprié
				switch (requete.keyWord) {
					case "interested":
					case "have":
						response = "have " + requete.fileKey + " ";
						//Recupère la bufferMap du pair au quel le handler est associé
						Object[] bufferMap = linkedPeer.buffermapFromKey(requete.fileKey, 1024);
						for (int i = 0; i < (int) bufferMap[1]; i++) {
							if (((BitSet) bufferMap[0]).get(i))
								response += "1";
							else
								response += "0";
						}
						break;
					case "getpieces":
						response = "data " + requete.fileKey + " [";

						for (String x : requete.indexes) {
							byte[] piece = linkedPeer.pieceFromKey(requete.fileKey, Integer.parseInt(x), 1024);
							response += x + ":" + "%" + new String(piece) + "%" + " ";
						}
						response = response.trim() + "]";
						break;

					case "close":
						System.out.println("Client closes the connection stream");
						closeConnexion();
						return;
					default:
						break;
				}
				response += '\n';
				System.out.println(/*"To " + socket.getPort() + " : " + */"< " + response);
				bytesToSend = response.getBytes();
				//Envoie de la reponse
				Thread.sleep(3000);
				this.output.write(bytesToSend, 0, bytesToSend.length);
				this.output.flush();
			} catch (Exception e) {
				System.out.println("Dans handler run : Communication échouée");
			}

		}
	}

}
