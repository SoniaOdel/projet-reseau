import java.net.*;
import java.io.*;
import java.util.*;
import javax.crypto.*;
import java.security.*;
import java.math.BigInteger;



/* Précision : le BitSet représente la buffermap du fichier,
contrairement à ce que son nom indique ce n'est pas une séquence de
0 et 1 mais une sorte de tableau contenant les indices des pièces
présentes du fichier.

Chaque FileManager a en attribut un LocalFile, le LocalFile est là ou se passe
le découpage des fichiers détenus par le paire initialement.
Le but du LocalFile est de créer un répertoire peer_IDpeer dans lequel seront
stockés les parties détenues par le paire.

Au début, si on veut que notre paire partage un fichier, on le place dans le
répertoire peer_IDpeer/files, il existe une fonction dans LocalFile pour le découper
en pieces. On le découpe en pièce comme ça, si un autre paire souhaite obtenir une partie
on l'obtient directement via la méthode getPiece(index).

Executer la classe FileManager pour avoir un apperçu de l'affichage de la buffermap
propre au fichier, et de la méthode hasPart(i) qui retourne vrai ou faux selon est
ce qu'un fichier détient une partie.
*/

public class FileManager {

    final private int peerID;
    final int pieceLength;
    final private int buffermapSize;
    private BitSet buffermapFile;
    private LocalFile file;
    private String fileName;
    private String fileKey;
    private long fileLength;

    public FileManager(int peerID, String fileName, int pieceLength) throws NoSuchAlgorithmException {

      this.file = new LocalFile(peerID,fileName,pieceLength);
      this.peerID = peerID;
      this.fileName = fileName;
      this.pieceLength = pieceLength;
      this.fileLength = this.getFileLength();
      this.buffermapSize = (int)(fileLength / pieceLength)+1;
      this.buffermapFile = new BitSet(buffermapSize);
      this.buffermapFile = initBuffermap(peerID);

      //On génère la clé à partir du nom du fichier + sa longueur avec md5sum
      String nameAndLength = fileName + fileLength;
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      byte[] digest = md5.digest(nameAndLength.getBytes());
      BigInteger no = new BigInteger(1, digest);
      String cle = no.toString(16);
      while (cle.length() < 32) {
        cle = "0" + cle;
      }
      this.fileKey = cle;

    }

    public BitSet initBuffermap(int peerID){
      String path = "./src/peer/peersfiles/peer" + peerID + "/files/pieces/" + fileName;
      String[] pieces;
      File f = new File(path);
      pieces = f.list();
      for (String piece : pieces){
        buffermapFile.set(Integer.parseInt(piece));
      }
      return buffermapFile;
    }

    public String getKey(){
      return fileKey;
    }

    public long getFileLength(){
      String directoryPath = "./src/peer/peersfiles/peer" + this.peerID + "/files/pieces/" + fileName;
      long length = 0;
      File dir = new File(directoryPath);
      File[] fileListing = dir.listFiles();
      for (File child : fileListing){
        length += child.length();
      }
      return length;
    }

    public long getPieceLength(){
      return this.pieceLength;
    }

    public String getName(){
      return this.fileName;
    }

    public int getBufferMapSize(){
      return this.buffermapSize;
    }
    public BitSet getBufferMapFile(){
      return this.buffermapFile;
    }

    public byte[] getPiece(int pieceIndex)throws NoSuchAlgorithmException, IOException{
      byte[] wantedPiece = this.file.getPiece(pieceIndex);
      return wantedPiece;
    }


    public boolean hasPart(int pieceIndex){
        return buffermapFile.get(pieceIndex);
    }

    public boolean hasParts(int[] piecesTab){
        for (int i = 0; i < piecesTab.length; i++){
          if (!this.hasPart(piecesTab[i]))
            return false;
        }
        return true;
    }

    public boolean isFileComplete() {
    for (int i = 0; i < buffermapSize; i++) {
        if (!buffermapFile.get(i)) {
            return false;
        }
    }
    return true;
    }

    public void setLength(long length){
	     this.fileLength = length;
    }

    public void addPiece(int pieceIndex, byte[] piece) throws IOException{
      final boolean isNewPiece = !buffermapFile.get(pieceIndex);
      buffermapFile.set(pieceIndex);
      file.addPiece(piece, pieceIndex);
    }

    public static void main(String args[])throws NoSuchAlgorithmException, IOException, FileNotFoundException{
      FileManager fm = new FileManager(1001, "Dali.txt", 1024);
      FileManager fm2 = new FileManager(1001, "DeVinci.txt", 1024);
      System.out.println("Dali's file key : " + fm.fileKey);
      System.out.println("De Vinci's key : " + fm2.fileKey);
      System.out.println("Dali's file length : " + fm.fileLength);
      System.out.println("Buffermap is : " + fm.buffermapFile + " buffermapSize is " + fm.buffermapSize + " and file complete is at : " + fm.isFileComplete());
      System.out.println("Does De Vinci have piece 2 ? : " + fm.hasPart(2));
      System.out.println("De Vinci's file length : " + fm2.fileLength);
      System.out.println("De Vinci's file : " + fm2.buffermapFile);
      System.out.println("Does de Vinci have part 2 : " + fm2.hasPart(2));
    }
}
