/*
 *Fichier de traitement des requetes
 */
//package peer;
import java.lang.*;
import java.util.*;
import java.util.BitSet;


public class Requete{
    String keyWord;
    String fileKey;
    String[] indexes;
    BitSet bufferMap;
    Hashtable<Integer,String> peers;
    Hashtable<Integer,byte[]> data;
    String dat;
    
    public Requete(){
    }
    
    public boolean integrity(String str){
	
	String[] tmp =  str.split("\\s+(?![^\\[]*\\])");
	this.keyWord = tmp[0];

	switch(this.keyWord){
	case "interested":
		if(tmp.length != 2)
			return false;
	    this.fileKey = tmp[1];
	    break;
	case "getpieces":
		if(tmp.length != 3)
			return false;
	    this.fileKey = tmp[1];
	    indexes = tmp[2].split(" ");
		if(indexes.length == 0)
			return false;
	    indexes[0] = indexes[0].substring(1,indexes[0].length());//""+indexes[0].charAt(1);
	    indexes[indexes.length-1] = indexes[indexes.length-1].substring(0,indexes[indexes.length-1].length()-1);//""+indexes[indexes.length-1].charAt(0);
	    break;
	case "have":
	    this.fileKey = tmp[1];
	    String s = tmp[2];
	    bufferMap = toBitset(s);
	    //si la taille du buff n'est pas coherente à la taille du fichier return false
	    break;
	case "peers":
	    this.fileKey = tmp[1];
	    indexes = tmp[2].split(" ");
	    indexes[0] = indexes[0].substring(1,indexes[0].length());
	    indexes[indexes.length-1] = indexes[indexes.length-1].substring(0,indexes[indexes.length-1].length()-1);
	    if (indexes.length == 0) return false;
	    peers = new Hashtable<Integer,String>();
	    for (int i = 0; i < indexes.length; i++){
		String[] st = indexes[i].split(":");
		peers.put(Integer.parseInt(st[1]),st[0]);
	    }
	    break;
	case "list":
	    indexes = tmp[1].split(" ");
	    indexes[0] = indexes[0].substring(1,indexes[0].length());
	    indexes[indexes.length-1] = indexes[indexes.length-1].substring(0,indexes[indexes.length-1].length()-1);
	    if (indexes.length == 0) return false;
	    break;
	case "data":
	    this.fileKey = tmp[1];
	    dat ="";
	    for (int i = 2; i < tmp.length; i++) {
		dat += tmp[i] + " ";
	    }
	    dat = dat.trim();
	    indexes = dat.split("%");
	    indexes[0] = indexes[0].substring(1,indexes[0].length()-1);
	    indexes[indexes.length-1] = indexes[indexes.length-1].substring(0,indexes[indexes.length-1].length()-1);
	    data = new Hashtable<Integer,byte[]>();
	    int ind = Integer.parseInt(indexes[0]);
	    data.put(ind, indexes[1].getBytes());
	    for (int i = 1; i < indexes.length/2; i++) {
		indexes[2*i] = indexes[2*i].substring(1, indexes[2*i].length()-1);
		data.put(Integer.parseInt(indexes[2*i]), indexes[2*i+1].getBytes());
	    }
	default:
	    //  return false;
	}
	return true;
    }    

    public BitSet toBitset(String s){
	char[] tochar = s.toCharArray();
	int len = tochar.length;
	BitSet ret = new BitSet(len);
	try {
	    for (int i = 0; i < len; i++) {
		if (Integer.parseInt(String.valueOf(tochar[i])) == 1)
		    ret.set(i);
	    }
	}
	catch(Exception e){
	    //System.out.println("Exception in toboolean");
	}
	return ret;
    }

    public void usage(){
	System.out.println("usage");
	
    }
}
    


/*    
      public int[] string2Int(String[] s){
      int len = s.length;
      int[] ret = new int[len];
      try{
      for(int i=0;i<len;i++)
      ret[i] = Integer.parseInt(s[i]);
      }
      catch(Exception e){
      System.out.println("Exception in string2Int");
      }
      return ret;
      }

      public boolean[] toBoolean(byte[] b){
      boolean[] ret = new boolean[b.length*8];
      int pos = 0;
      for(byte x : b){
      boolean[] tmp = singleByte2Boolean(x);
      System.arraycopy(tmp,0,ret,pos,8);
      pos +=8;
      }
      return ret;
      }

    	    
      public boolean[] singleByte2Boolean(byte b){
      boolean bl[] = new boolean[8];
      bl[0] = ((b & 0x01) != 0);
      bl[1] = ((b & 0x02) != 0);
      bl[2] = ((b & 0x03) != 0);
      bl[3] = ((b & 0x04) != 0);
      bl[4] = ((b & 0x05) != 0);
      bl[5] = ((b & 0x06) != 0);
      bl[6] = ((b & 0x07) != 0);
      bl[7] = ((b & 0x08) != 0);
      return bl;
      }

      public byte[] toByte(boolean[] input){
      byte[] output = new byte[input.length/8];
      for(int entry=0; entry<output.length; entry++){
      for(int bit=0; bit<8; bit++){
      if(input[entry*8+bit]){
      output[entry] |= (128 >> bit);
      }
      }
      }
      return output;
      }
*/	
    
