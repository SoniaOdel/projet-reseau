/*
	Le server est le centre d'écoute pour un pair.
	Il recoit les connections entrantes
	Il possède un pools de thread, les handlers qui s'occupe réellement de la communication.
	Lorsqu'une connection est acceptée, elle est placée en file d'attente et traité dès qu'un handler
	est libre pour s'en occuper.
 */
import java.net.*;
import java.util.*;
import java.lang.*;

public class Server extends Thread {
    private ServerSocket serverSocket;
	private int maxHandlers;
	private int maxWaitingConn;
    private ArrayList<Handler> handlers; //pool de threads
    private LinkedList<Socket> waitingConn; //Liste d'attente des connexions entrantes

    public Server(String adressIp, int port, int maxHandlers, int maxWaitingConn, Client p) {
		this.maxHandlers = maxHandlers;
		this.maxWaitingConn = maxWaitingConn;
		//initialisation des handlers
		this.handlers = new ArrayList<Handler>(maxHandlers);
		for (int i = 0; i < this.maxHandlers; i++) {
			Handler tmp = new Handler(p); //un handler est instancié avec un pair en parametre un pair.
			handlers.add(tmp);            //Cela permet au handler de savoir a quel pair il est rattaché
			// notamment pour envoie de données comme buffermap
		}
		//Initialisation de la liste de connextions en attente d'etre traitées
		this.waitingConn = new LinkedList<Socket>();

		//Creation de la socket d'écoute sur le port et adresse ip en parametre.
		try {
			this.serverSocket = new ServerSocket(port, 10, InetAddress.getByName(adressIp));
		} catch (Exception e) {
			System.out.println("Instanciation impossible du server");
		}
	}

    //Fonction principal du server. Methode run exécutée lors de l'appel à start
    //On accepte les connexions entrantes et gère leur affectation a un handler libre
    public void run() {
		System.out.println("Server lauched");
		while (true) {
			Socket socket;
			this.manageHandlers();
			if (waitingConn.size() < maxWaitingConn) //on accepte les connexions tant que le maximum de
				try {                                    //connexions entrantes n'est pas atteint
					socket = this.serverSocket.accept();
					waitingConn.add(socket);            //Des q'une connexion est acceptée, on la place dans la file d'attente
				} catch (Exception e) {
					System.out.println("Le server ne peut accepter de connexion");
				}
		}
	}

    //Fonction de recherche de handler libre dans la file
    //Return null si pas de handler libre
    private Handler getFreeHandler() {
	Handler handler;
	Iterator<Handler> tmp = this.handlers.iterator();
	while (tmp.hasNext()) {
	    handler = tmp.next();
	    if (handler.isfree()) {
			return handler;
	    }
	}
	return null;
    }

    //Fonction d'assignation de travail à un handler libre
    private void manageHandlers() {
	Handler handler = this.getFreeHandler();
	if (!waitingConn.isEmpty() && handler != null) {
	    try {
	    	Socket socket = waitingConn.removeFirst();
		System.out.println("------------------------------------");
		System.out.println("New connection accepted");// \n<Pair ip:" + socket.getInetAddress().getHostAddress() + " port:"+socket.getPort() +">");
		System.out.println("------------------------------------");
		handler.dealWith(socket);
	    } catch (Exception e) {
	    	System.out.println("Exception in manageHandlers in classe Server");
	    }
	}
    }
}
