#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash_table.h"
#include "client_manager.h"

/**
  * Computes the hash of a string
  * @param str String to hash
  * @return the hash value
  */
unsigned long hash_function(const char* const str) {
    unsigned long hash = PRIME_1;
    for (size_t i = 0; i < strlen(str); i++) {
      hash = hash*PRIME_2 + str[i];
    }
    return hash % CAPACITY;
}

/**
  * Allocates memory on the heap for a linkedlist_item
  * @return a pointer to the new linkedlist_item
  */
linkedlist_item* allocate_list_item () {
    linkedlist_item* list = calloc(1, sizeof(*list));
    return list;
}

/**
  * Inserts an item at the end of a linkedlist_item
  * @param list Linked list to insert into
  * @param item Item to insert
  * @return a pointer to the linked list
  */
linkedlist_item* linkedlist_item_insert(linkedlist_item* list,
                                        ht_item* const item) {
    if (!list) {
        linkedlist_item* head = allocate_list_item();
        head->item = item;
        head->next = NULL;
        list = head;
        return list;
    }

    else if (list->next == NULL) {
        linkedlist_item* node = allocate_list_item();
        node->item = item;
        node->next = NULL;
        list->next = node;
        return list;
    }

    linkedlist_item* temp = list;
    while (temp->next->next) {
        temp = temp->next;
    }

    linkedlist_item* node = allocate_list_item();
    node->item = item;
    node->next = NULL;
    temp->next = node;

    return list;
}

/**
  * Removes the head from the linkedlist_item
  * @param list Linked list to remove from
  * @return the item of the popped element
  */
ht_item* linkedlist_item_remove(linkedlist_item* list) {
    if (!list)
        return NULL;
    if (!list->next)
        return NULL;
    linkedlist_item* node = list->next;
    linkedlist_item* temp = list;
    temp->next = NULL;
    list = node;
    ht_item* it = NULL;
    memcpy(temp->item, it, sizeof(ht_item));
    free(temp->item->name);
    free(temp->item->key);
    free(temp->item->seeders);
    free(temp->item->leechers);
    free(temp->item);
    free(temp);
    return it;
}

/**
  * Frees all items of a linkedlist_item
  * @param list Linked list to free
  */
void free_linkedlist_item(linkedlist_item* list) {
    linkedlist_item* temp = list;
    if (!list)
        return;
    while (list) {
        temp = list;
        list = list->next;
        free(temp->item->name);
        free(temp->item->key);
        free(temp->item);
        free(temp);
    }
}

/**
  * Creates the overflow buckets; an array of linked lists
  * @param table Hash table to associate the buckets to
  * @return a pointer to the overflow buckets
  */
linkedlist_item** create_overflow_buckets(const hash_table* const table) {
    linkedlist_item** buckets = calloc(table->size, sizeof(*buckets));
    for (size_t i=0; i<table->size; i++)
        buckets[i] = NULL;
    return buckets;
}

/**
  * Frees all the overflow bucket lists
  * @param table Hash table whose buckets must be freed
  */
void free_overflow_buckets(const hash_table* const table) {
    linkedlist_item** buckets = table->overflow_buckets;
    for (size_t i=0; i<table->size; i++)
        free_linkedlist_item(buckets[i]);
    free(buckets);
}

/**
  * Creates a new hash table item
  * @param key         Key of the file
  * @param name        Name of the file
  * @param size        Size of the file
  * @param pieces_size Size of the fragments of the file
  * @return a pointer to the new item
  */
ht_item* create_item(const char* const key, const char* name,
                     const size_t size, const size_t pieces_size) {
    ht_item* item = malloc(sizeof(*item));

    if(name == NULL)
      name = "Unknown";

    item->key = calloc(strlen(key) + 1, sizeof(*item->key));
    item->name = calloc(strlen(name) + 1, sizeof(*item->name));
    item->seeders = NULL;
    item->leechers = NULL;

    strcpy(item->key, key);
    strcpy(item->name, name);
    item->size = size;
    item->pieces_size = pieces_size;

    return item;
}

/**
  * Creates a new hash table
  * @param size Size of the table
  * @return a pointer to the new hash table
  */
hash_table* create_table(const size_t size) {
    hash_table* table = malloc(sizeof(*table));
    table->size = size;
    table->count = 0;
    table->items = calloc(table->size, sizeof(*table->items));
    for (size_t i=0; i<table->size; i++)
        table->items[i] = NULL;
    table->overflow_buckets = create_overflow_buckets(table);

    return table;
}

/**
  * Frees a hash table item
  * @param item Item to free
  */
static void free_item(ht_item* const item) {
    free(item->name);
    free(item->key);
    free_linkedlist_client(item->seeders);
    free_linkedlist_client(item->leechers);
    free(item);
}

/**
  * Frees a hash table
  * @param item Item to free
  */
void free_table(hash_table* const table) {
    for (size_t i=0; i<table->size; i++) {
        ht_item* item = table->items[i];
        if (item != NULL)
            free_item(item);
    }
    free_overflow_buckets(table);
    free(table->items);
    free(table);
}

/**
  * Places hash collisions (duplicates) in buckets
  * @param table Hash table to insert into
  * @param index Hash key of the item
  * @param item  Item to insert
  */
void handle_collision(const hash_table* const table,
                      const unsigned long index,
                      ht_item* const item) {
    linkedlist_item* head = table->overflow_buckets[index];

    if (head == NULL) {
        // We need to create the list
        head = allocate_list_item();
        head->item = item;
        table->overflow_buckets[index] = head;
        return;
    }
    else {
        // Insert to the list
        table->overflow_buckets[index] = linkedlist_item_insert(head, item);
        return;
    }
 }

 /**
   * Places a new item (file) in a hash table
   * @param table       Hash table to insert into
   * @param key         Key of the file to insert
   * @param name        Name of the file to insert
   * @param size        Size of the file to insert
   * @param pieces_size Size of the fragments of the file to insert
   * @param seeder      Client seeding the file or NULL
   * @param leecher     Client leeching the file or NULL
   */
void ht_insert(hash_table* const table, const char* const key,
               const char* const name, const size_t size,
               const size_t pieces_size,
               client* const seeder, client* const leecher) {
    // Create the item
    ht_item* item = create_item(key, name, size, pieces_size);

    // Compute the index
    int index = hash_function(key);

    ht_item* current_item = table->items[index];

    if (current_item == NULL) {
        // Key does not exist.
        if (table->count == table->size) {
            // Hash Table Full
            printf("Insert Error: Hash Table is full\n");
            // Remove the create item
            free_item(item);
            return;
        }

        // Insert directly
        if(seeder != NULL)
          item->seeders = linkedlist_client_insert(item->seeders, seeder);
        if(leecher != NULL)
          item->leechers = linkedlist_client_insert(item->leechers, leecher);
        table->items[index] = item;
        table->count++;
    } else {

        // Scenario 1: We only need to update informations
        if (strcmp(current_item->key, key) == 0) {
            if(name != NULL){
              free(current_item->name);
              current_item->name = calloc(strlen(name) + 1, sizeof(*name));
              strcpy(current_item->name, name);
            }

            current_item->size = size;
            current_item->pieces_size = pieces_size;

            if(seeder != NULL)
              current_item->seeders =
                linkedlist_client_insert(current_item->seeders, seeder);
            if(leecher != NULL)
              current_item->leechers =
                linkedlist_client_insert(current_item->leechers, leecher);

            free_item(item);
            return;
        } else {
            // Scenario 2: Collision
            handle_collision(table, index, item);
            return;
        }
    }
}

/**
  * Updates the seeders and/or leechers of an item (file)
  * @param table       Hash table to insert into
  * @param key         Key of the file to insert
  * @param seeder      Client seeding the file or NULL
  * @param leecher     Client leeching the file or NULL
  */
void ht_update(hash_table* const table, const char* const key,
              client* const seeder, client* const leecher) {

   // Find the file
   ht_item* item = ht_search(table, key);

   // Unknown file
   if(!item)
    return;

   if(seeder != NULL){
     item->seeders =
       linkedlist_client_update(item->seeders, seeder);
   }

   if(leecher != NULL){
     item->leechers =
       linkedlist_client_update(item->leechers, leecher);
   }
}

/**
  * Searches for an item in a hash table, based on its key
  * @param table Hash table to search in
  * @param key   Key of the file to search for
  * @return the item if it exists, NULL otherwise
  */
ht_item* ht_search(const hash_table* const table, const char* const key) {
    int index = hash_function(key);
    ht_item* item = table->items[index];
    linkedlist_item* head = table->overflow_buckets[index];

    // Ensure that we move to items which are not NULL
    while (item != NULL) {
        if (strcmp(item->key, key) == 0)
            return item;
        if (head == NULL)
            return NULL;
        item = head->item;
        head = head->next;
    }
    return NULL;
}

/**
  * Copies all item keys in a specified array
  * @param table Hash table to copy from
  * @param files Array of strings to copy into
  * @param size  Size of the array of strings
  */
void ht_retrieve_all(const hash_table* const table,
                     char** const files, const size_t size) {
    size_t cpt = 0;
    for(size_t index = 0; index < CAPACITY; index++) {
        if((table->items[index] != NULL) && (cpt < size)){
          files[cpt] = table->items[index]->key;
          cpt++;
        }
    }
}

/**
  * Deletes an item from a table
  * @param table Hash table to delete from
  * @param key   Key of the item to delete
  */
void ht_delete(hash_table* const table, const char* const key) {
    int index = hash_function(key);
    ht_item* item = table->items[index];
    linkedlist_item* head = table->overflow_buckets[index];

    if (item == NULL) {
        // Does not exist
        return;
    }
    else {
        if (head == NULL && strcmp(item->key, key) == 0) {
            // No collision chain. Remove the item
            // and set table index to NULL
            table->items[index] = NULL;
            free_item(item);
            table->count--;
            return;
        }
        else if (head != NULL) {
            // Collision Chain exists
            if (strcmp(item->key, key) == 0) {
                // Remove this item and set the head of the list
                // as the new item
                free_item(item);
                linkedlist_item* node = head;
                head = head->next;
                node->next = NULL;
                table->items[index] = create_item(
                  node->item->key,
                  node->item->name,
                  node->item->size,
                  node->item->pieces_size);
                table->items[index]->seeders = node->item->seeders;
                table->items[index]->leechers = node->item->leechers;
                free_linkedlist_item(node);
                table->overflow_buckets[index] = head;
                return;
            }

            linkedlist_item* curr = head;
            linkedlist_item* prev = NULL;
            while (curr) {
                if (strcmp(curr->item->key, key) == 0) {
                    if (prev == NULL) {
                        // First element of the chain. Remove the chain
                        free_linkedlist_item(head);
                        table->overflow_buckets[index] = NULL;
                        return;
                    }
                    else {
                        // This is somewhere in the chain
                        prev->next = curr->next;
                        curr->next = NULL;
                        free_linkedlist_item(curr);
                        table->overflow_buckets[index] = head;
                        return;
                    }
                }
                curr = curr->next;
                prev = curr;
            }
        }
    }
}

/**
  * Displays an item information onto stdout
  * @param item Item to display
  */
void print_item(ht_item* it){
  printf("Key:%s, Name:%s, Size:%lu, Pieces size:%lu\n",
    it->key, it->name, it->size, it->pieces_size);
  printf("\tSeed:\n");
  linkedlist_client* c = it->seeders;
  while(c != NULL){
    printf("\t\t%d %s %d\n", c->client->id, c->client->ip, c->client->port);
    c = c->next;
  }
  printf("\tLeech:\n");
  c = it->leechers;
  while(c != NULL){
    printf("\t\t%d %s %d\n", c->client->id, c->client->ip, c->client->port);
    c = c->next;
  }
}

/**
  * Displays the result of the ht_search() function onto stdout
  * @param table Hash table to search in
  * @param key   Key of the file to search for
  */
void print_search(const hash_table* const table, const char* const key) {
    ht_item* it;
    if ((it = ht_search(table, key)) == NULL) {
        printf("%s does not exist\n", key);
        return;
    }
    else {
        print_item(it);
    }
}

/**
  * Displays a hash table information onto stdout
  * @param table Table to display
  */
void print_table(const hash_table* const table) {
    printf("\n-------------------\n");
    for (size_t i=0; i<table->size; i++) {
        if (table->items[i]) {
            printf("Index:%lu, ", i);
            print_item(table->items[i]);
            if (table->overflow_buckets[i]) {
                printf(" => Overflow Bucket => ");
                linkedlist_item* head = table->overflow_buckets[i];
                while (head) {
                    print_item(head->item);
                    head = head->next;
                }
            }
            printf("\n");
        }
    }
    printf("-------------------\n");
}

/**
  * Displays a hash table information onto stdout
  * @param table Table to display
  */
size_t ht_get_count(const hash_table* const table){
  return table->count;
}
