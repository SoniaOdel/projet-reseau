#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "parser.h"
#include "hash_table.h"
#include "memory_manager.h"
#include "client_manager.h"

/* Ensures uniqueness of client id */
unsigned int id = 0;

/**
  * Initializes the parser environment
  */
void init_parser(){
  // Initializes the linked list of known clients
  SLIST_INIT(&head_lc);
  //Initializes the hash table of known files
  ht = create_table(CAPACITY);
}

/**
  * Checks if a string is a known command
  * @param s String to check
  * @return the id of the command if it exists, -1 otherwise
  */
enum command id_command(const char* const s){
  if(!strcmp(s, "announce"))
      return ANNOUNCE;
  if(!strcmp(s, "listen"))
      return LISTEN;
  if(!strcmp(s, "seed"))
      return SEED;
  if(!strcmp(s, "leech"))
      return LEECH;
  if(!strcmp(s, "update"))
    return UPDATE;
  if(!strcmp(s, "getfile"))
      return GETFILE;
  if(!strcmp(s, "look"))
      return LOOK;
  return -1;
}

/**
  * Checks if a string is a known comparator
  * @param s String to check
  * @return the id of the comparator if it exists, -1 otherwise
  */
enum comparator id_comparator(const char* const s){
  if(!strcmp(s, "<"))
    return LT;
  if(!strcmp(s, ">"))
    return GT;
  if(!strcmp(s, "="))
    return EQ;
  if(!strcmp(s, "!="))
    return NEQ;
  if(!strcmp(s, "<="))
    return LEQ;
  if(!strcmp(s, ">="))
    return GEQ;
  return -1;
}

/**
  * Checks if a string is a known field name
  * @param s String to check
  * @return the id of the field if it exists, -1 otherwise
  */
enum field id_field(const char* const s){
  if(!strcmp(s, "filename"))
    return NAME;
  if(!strcmp(s, "filesize"))
    return SIZE;
  if(!strcmp(s, "piecessize"))
    return P_SIZE;
  if(!strcmp(s, "filekey"))
    return KEY;
  return -1;
}

/**
  * Gets the number of files in the hash table
  * @return the number of files in the hash table
  */
size_t files_number(){
  return ht_get_count(ht);
}

/**
  * Checks if a file respects all given constraints
  * @param file_key       Key of the file
  * @param fields         Array of fields of constraints
  * @param cmps           Array of comparators of constraints
  * @param values         Array of values of constraints
  * @param nb_constraints Number of constraints
  * @return true if the file respects the constraint, false otherwise
  */
unsigned short respect_constraints(char* const file_key,
                                   char** fields,
                                   char** cmps,
                                   char** values,
                                   const size_t nb_constraints){
  int res = 0;
  // Recovers the file from its key
  file_p2p* file = ht_search(ht, file_key);
  char* file_str = malloc(sizeof(*file_str) * MAX_LEN_FILENAME);

  // For each constraints, makes the comparison with the appropriate field
  for(size_t i = 0; i < nb_constraints; i++){
    switch (id_field(fields[i])) {
      case NAME:
        res = strcmp(file->name, values[i]);
        break;
      case SIZE:
        sprintf(file_str, "%lu", file->size);
        res = strcmp(file_str, values[i]);
        break;
      case P_SIZE:
        sprintf(file_str, "%lu", file->pieces_size);
        res = strcmp(file_str, values[i]);
        break;
      case KEY:
        res = strcmp(file->key, values[i]);
        break;
      default:
        free(file_str);
        return 0;
    }

    // strcmp() can return 3 types of values:
    // - 0: string are equals
    //   -> comparators "=", "<=" and ">="
    // - <0 : character of greater value in string1 than in string2
    //   -> comparators "!=" and "<"
    // - >0 : character of lower value in string1 than in string2
    //   -> comparators "!=" and ">"
    enum comparator id_cmp = id_comparator(cmps[i]);
    if(!(((!res && ((id_cmp == EQ) || (id_cmp == LEQ) || (id_cmp == GEQ)))) ||
        (((res < 0) && ((id_cmp == NEQ) || (id_cmp == LT)))) ||
        (((res > 0) && ((id_cmp == NEQ) || (id_cmp == GT)))))){
      free(file_str);
      // If one constraint is not respected, it stops
      return 0;
    }
  }
  free(file_str);
  return 1;
}

/**
  * Checks if each file respects all given constraints
  * @param files          Array of the file keys respecting the constraint
  * @param size_files     Size of the array
  * @param nb_files       Number of files in the array
  * @param fields         Array of fields of constraints
  * @param cmps           Array of comparators of constraints
  * @param values         Array of values of constraints
  * @param nb_constraints Number of constraints
  */
void check_constraints(char** const files, const size_t size_files,
                       size_t* nb_files,
                       char**  fields,
                       char** cmps,
                       char** values,
                       const size_t nb_constraints){
  size_t cpt = 0;
  unsigned int i = 0;
  // For each existing file in files[], checks all the constraints
  while((i < size_files) && (cpt < *nb_files)){
    if(files[i] != NULL) {
      if(!respect_constraints(files[i], fields, cmps, values, nb_constraints)){
        // If it doesn't respect all the constraints, it's deleted from files[]
        files[i] = NULL;
        (*nb_files)--;
      } else
        cpt++;
    }
    i++;
  }
}

/**
  * Checks if character is used for comparison
  * @param c Character to test
  * @return true if it is a comparison character, false otherwise
  */
unsigned short is_cmp(const char c){
  size_t size = strlen(cmp_chars);
  for(unsigned int i = 0; i < size; i++){
    if(cmp_chars[i] == c)
      return 1;
  }
  return 0;
}

/**
  * Formats the answer to the look command
  * @param files       Array of file keys
  * @param size_files  Size of the files array
  * @param answer      Pointer to a string containing the answer
  * @param answer_size Size of the answer string
  * @return the new size of the answer string
  */
size_t answer_look(char** const files, const size_t size_files,
                   char** const answer, size_t answer_size){
  // We have to use res and tmp_res because the variable sprintf()
  // is assigned to cannot be the same as one of its argument
  size_t res_size = DEFAULT_MALLOC_SIZE;
  char* res = malloc(sizeof(*res) * res_size);
  size_t tmp_res_size = DEFAULT_MALLOC_SIZE;
  char* tmp_res = malloc(sizeof(*tmp_res) * tmp_res_size);
  size_t files_nb = 0;

  char* s_begin = "list [";
  res_size = copy_string(s_begin, strlen(s_begin), &res, res_size);

  for(unsigned int i = 0; i < size_files; i++){
    if(files[i] != NULL){
      file_p2p* file = ht_search(ht, files[i]);
      if(file != NULL){
        if(files_nb == 0){
          // First checks if enough memory is allocated before copying
          tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
            snprintf(NULL, 0, "%s%s %lu %lu %s", res,
            file->name, file->size,
            file->pieces_size, file->key) + 1);
          sprintf(tmp_res, "%s%s %lu %lu %s",
            res, file->name, file->size, file->pieces_size, file->key);
          res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
        } else {
          // First checks if enough memory is allocated before copying
          tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
            snprintf(NULL, 0, "%s %s %lu %lu %s", res,
            file->name, file->size,
            file->pieces_size, file->key) + 1);
          sprintf(tmp_res, "%s %s %lu %lu %s",
            res, file->name, file->size, file->pieces_size, file->key);
          res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
        }
        files_nb++;
      }
    }
  }

  // First checks if enough memory is allocated before copying
  tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
    snprintf(NULL, 0, "%s]\n", res) + 1);
  sprintf(tmp_res, "%s]\n", res);
  res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
  printf("%s", res);
  answer_size = copy_string(res, res_size, answer, answer_size);

  free(res);
  free(tmp_res);

  return answer_size;
}

/**
  * Parses a look command
  * @param saveptr     Pointer to the saved string token (see man strtok_r)
  * @param answer      Pointer to a string containing the answer
  * @param answer_size Size of the answer string
  * @return the new size of the answer string
  */
size_t process_look(char* saveptr, char** const answer, size_t answer_size){
  char* token;
  size_t size_files = files_number();
  size_t nb_files = size_files;
  char* files[size_files];
  ht_retrieve_all(ht, files, size_files);
  char** fields = malloc(DEFAULT_MALLOC_SIZE * sizeof(*fields));
  char** cmps = malloc(DEFAULT_MALLOC_SIZE * sizeof(*cmps));
  char** values = malloc(DEFAULT_MALLOC_SIZE * sizeof(*values));
  size_t size_constraints = DEFAULT_MALLOC_SIZE;
  size_t nb = 0;

  // For each constraint, gets the field, comparator and value of comparison
  // and stocks it in the right array
  for (token = strtok_r(NULL, delim, &saveptr);
      token != NULL;
      token = strtok_r(NULL, delim, &saveptr)) {
    size_t size_tok = strlen(token);
    size_t size_field, size_cmp, size_value;
    unsigned int i = 0;

    // Checks if the array of constraints needs to be resized
    while(nb > (size_constraints-1)){
      double_alloc_size_str(fields, size_constraints);
      double_alloc_size_str(cmps, size_constraints);
      size_constraints = double_alloc_size_str(values, size_constraints);
    }

    fields[nb] = malloc(size_tok);
    cmps[nb] = malloc(size_tok);
    values[nb] = malloc(size_tok);

    // Skips the '[' for the first field
    if(nb == 0)
      i++;

    // Gets the ith field of comparison
    size_field = 0;
    while((i < size_tok) && !is_cmp(token[i])){
      fields[nb][size_field] = token[i];
      i++;
      size_field++;
    }
    fields[nb][size_field] = '\0';

    // Gets the ith comparator of comparison
    size_cmp = 0;
    while((i < size_tok) && is_cmp(token[i])){
      cmps[nb][size_cmp] = token[i];
      i++;
      size_cmp++;
    }
    if(token[i] == '\"')
      i++;
    cmps[nb][size_cmp] = '\0';

    // Gets the ith value of comparison
    size_value = 0;
    while((i < size_tok) && (token[i] != '\"') && token[i] != ']'){
      values[nb][size_value] = token[i];
      i++;
      size_value++;
    }
    values[nb][size_value] = '\0';

    nb++;
  }

  // Checks all constraints on all files
  // Complexity = O(f*c)
  // with f the number of files and c the number of constraints
  check_constraints(files, size_files, &nb_files,
    fields, cmps, values, nb);

  // Frees the malloc because they are not needed anymore
  for(size_t i = 0; i < nb; i++){
    free(fields[i]);
    free(cmps[i]);
    free(values[i]);
  }
  free(fields);
  free(cmps);
  free(values);

  // Formats the answer
  return answer_look(files, size_files, answer, answer_size);
}

/**
  * Parses a getfile command
  * @param saveptr     Pointer to the saved string token (see man strtok_r)
  * @param answer      Pointer to a string containing the answer
  * @param answer_size Size of the answer string
  * @return the new size of the answer string
  */
size_t process_getfile(char* saveptr, char** const answer, size_t answer_size){
  char* searched_key = strtok_r(NULL, delim, &saveptr);

  // We have to use res and tmp_res because the variable sprintf()
  // is assigned to cannot be the same as one of its argument
  size_t res_size = DEFAULT_MALLOC_SIZE;
  char* res = malloc(sizeof(*res) * res_size);
  size_t tmp_res_size = DEFAULT_MALLOC_SIZE;
  char* tmp_res = malloc(sizeof(*tmp_res) * tmp_res_size);
  size_t cpt = 0;

  // First checks if enough memory is allocated before copying
  res_size = check_sprintf_size(&res, res_size,
    snprintf(NULL, 0, "peers %s [", searched_key) + 1);
  sprintf(res, "peers %s [", searched_key);

  // Gets the file in the hash table
  file_p2p* f = ht_search(ht, searched_key);
  // If the file exist
  if(f != NULL) {
    // First print clients owning the whole file
    linkedlist_client* seeder = f->seeders;
    while(seeder != NULL) {
      if(cpt == 0){
        // First checks if enough memory is allocated before copying
        tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
          snprintf(NULL, 0, "%s%s:%d", res,
          seeder->client->ip, seeder->client->port) + 1);
        sprintf(tmp_res, "%s%s:%d", res,
          seeder->client->ip, seeder->client->port);
        res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
        seeder = seeder->next;
        cpt++;
      } else {
        // First checks if enough memory is allocated before copying
        tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
          snprintf(NULL, 0, "%s %s:%d", res,
          seeder->client->ip, seeder->client->port) + 1);
        sprintf(tmp_res, "%s %s:%d", res,
          seeder->client->ip, seeder->client->port);
        res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
        seeder = seeder->next;
      }
    }

    // Then print clients owning fragment(s) of the file
    linkedlist_client* leecher = f->leechers;
    while(leecher != NULL) {
      if(cpt == 0){
        // First checks if enough memory is allocated before copying
        tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
          snprintf(NULL, 0, "%s%s:%d", res,
          leecher->client->ip, leecher->client->port) + 1);
        sprintf(tmp_res, "%s%s:%d", res,
          leecher->client->ip, leecher->client->port);
        res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
        leecher = leecher->next;
        cpt++;
      } else {
        // First checks if enough memory is allocated before copying
        tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
          snprintf(NULL, 0, "%s %s:%d", res,
          leecher->client->ip, leecher->client->port) + 1);
        sprintf(tmp_res, "%s %s:%d", res,
          leecher->client->ip, leecher->client->port);
        res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
        leecher = leecher->next;
      }
    }
  }

  // First checks if enough memory is allocated before copying
  tmp_res_size = check_sprintf_size(&tmp_res, tmp_res_size,
    snprintf(NULL, 0, "%s]\n", res) + 1);
  sprintf(tmp_res, "%s]\n", res);
  res_size = copy_string(tmp_res, tmp_res_size, &res, res_size);
  printf("%s", res);
  answer_size = copy_string(res, res_size, answer, answer_size);

  free(res);
  free(tmp_res);

  return answer_size;
}

/**
  * Parses an announce command
  * @param saveptr     Pointer to the saved string token (see man strtok_r)
  * @param ip          IP of the client
  * @param answer      Pointer to a string containing the answer
  * @param answer_size Size of the answer string
  * @return the new size of the answer string
  */
size_t process_announce(char* saveptr, char* const ip,
                        char** const answer, const size_t answer_size){
  // Creates a new client each time an announce is parsed
  slist_client* lc = malloc(sizeof(*lc));
  client* c = malloc(sizeof(*c));
  c->id = id++;
  c->ip = ip;

  char* ptr = strtok_r(NULL, delim, &saveptr);
  // If the first command isn't "listen", then the query isn't valid
  // If it is, gets the port
  if(id_command(ptr) == LISTEN){
    ptr = strtok_r(NULL, delim, &saveptr);
    c->port = atoi(ptr);
    lc->client = c;
    SLIST_INSERT_HEAD(&head_lc, lc, next);
    ptr = strtok_r(NULL, delim, &saveptr);
  } else {
    free(c);
    return copy_string(ERROR_MSG_CMD, strlen(ERROR_MSG_CMD),
                       answer, answer_size);
  }

  // If the next command isn't "seed", then the query isn't valid
  if(id_command(ptr) == SEED){
    unsigned short is_first = 1;
    ptr = strtok_r(NULL, delim, &saveptr);
    // If the list of seed is empty, goes to the next command
    if(!strcmp(ptr, empty)) {
      ptr = strtok_r(NULL, delim, &saveptr);
    }
    // Otherwise gets the filename, the size, the size of the fragments
    // and the key of each file
    else {
      while(ptr != NULL && id_command(ptr) != LEECH)
      {
        // Gets the filename
        char* name = ptr;
        // If it's the first file, the filename would begin with a '['
        // that must be deleted
        if(is_first)
          name = name + sizeof(char);

        // Gets the file size
        ptr = strtok_r(NULL, delim, &saveptr);
        size_t size = atoi(ptr);

        // Gets the fragments size
        ptr = strtok_r(NULL, delim, &saveptr);
        size_t pieces_size = atoi(ptr);

        // Gets the key
        ptr = strtok_r(NULL, delim, &saveptr);
        size_t key_size = strlen(ptr);
        char key[key_size+1];
        memcpy(key, ptr, key_size+1);
        // If it's the last file, the key would end with a ']'
        // that must be deleted
        if(key[key_size-1] == ']'){
          key[key_size-1] = '\0';
        }

        // Inserts the file in the hash table
        ht_insert(ht, key, name, size, pieces_size, c, NULL);

        ptr = strtok_r(NULL, delim, &saveptr);
        is_first = 0;
      }
    }
  } else {
    free(c);
    return copy_string(ERROR_MSG_CMD, strlen(ERROR_MSG_CMD),
                       answer, answer_size);
  }

  // If the next command isn't "leech", then the query isn't valid
  if(id_command(ptr) == LEECH){
    unsigned short is_first = 1;
    ptr = strtok_r(NULL, delim, &saveptr);
    if(strcmp(ptr, empty)) {
      while(ptr != NULL){
        // Gets the key
        size_t key_size = strlen(ptr) - is_first;
        char key[key_size+1];
        // If it's the first file, the key would begin with a '['
        // that must be deleted
        int bgn_shift = sizeof(char) * is_first;
        memcpy(key, ptr + bgn_shift, key_size+1);
        // If it's the last file, the key would end with a ']'
        // that must be deleted
        if(key[key_size-1] == ']')
          key[key_size-1] = '\0';

        // Inserts the file in the hash table
        ht_insert(ht, key, NULL, 0, 0, NULL, c);

        is_first = 0;
        ptr = strtok_r(NULL, delim, &saveptr);
      }
    }
  } else {
    free(c);
    return copy_string(ERROR_MSG_CMD, strlen(ERROR_MSG_CMD),
                       answer, answer_size);
  }

  // Formats the answer
  char* a = "OK\n";
  printf("%s",a);
  return copy_string(a, strlen(a), answer, answer_size);
}

/**
  * Searches for a known client with a given IP adress
  * @param ip IP adress of the client to update
  * @return a pointer to the client if found, NULL otherwise
  */
slist_client* search_client_by_ip(const char* const ip){
  slist_client* c;
  SLIST_FOREACH(c, &head_lc, next) {
    if(!strcmp(c->client->ip, ip))
      return c;
  }
  return NULL;
}

/**
  * Parses an update command
  * @param saveptr     Pointer to the saved string token (see man strtok_r)
  * @param ip          IP of the client
  * @param answer      Pointer to a string containing the answer
  * @param answer_size Size of the answer string
  * @return the new size of the answer string
  */
size_t process_update(char* saveptr, char* const ip,
                      char** const answer, const size_t answer_size){
  // Gets the client of the query
  slist_client* lc = search_client_by_ip(ip);

  char* ptr = strtok_r(NULL, delim, &saveptr);

  // If the first command isn't "seed", then the query isn't valid
  if(id_command(ptr) == SEED){
    unsigned short is_first = 1;
    // If the list of seed is empty, goes to the next command
    if(!strcmp(ptr, empty)) {
      ptr = strtok_r(NULL, delim, &saveptr);
    }
    // Otherwise gets the key of each file
    else {
      while(ptr != NULL && id_command(ptr) != LEECH){
        // Gets the key
        size_t key_size = strlen(ptr) - is_first;
        char key[key_size+1];
        // If it's the first file, the key would begin with a '['
        // that must be deleted
        int bgn_shift = sizeof(char) * is_first;
        memcpy(key, ptr + bgn_shift, key_size+1);
        // If it's the last file, the key would end with a ']'
        // that must be deleted
        if(key[key_size-1] == ']')
          key[key_size-1] = '\0';

        // Updates the hash table with the new information
        ht_update(ht, key, lc->client, NULL);

        is_first = 0;
        ptr = strtok_r(NULL, delim, &saveptr);
      }
    }
  } else {
    return copy_string(ERROR_MSG_CMD, strlen(ERROR_MSG_CMD),
                       answer, answer_size);
  }

  // If the next command isn't "leech", then the query isn't valid
  if(id_command(ptr) == LEECH){
    unsigned short is_first = 1;
    ptr = strtok_r(NULL, delim, &saveptr);
    if(strcmp(ptr, empty)) {
      while(ptr != NULL){
        // Gets the key
        size_t key_size = strlen(ptr) - is_first;
        char key[key_size+1];
        // If it's the first file, the key would begin with a '['
        // that must be deleted
        int bgn_shift = sizeof(char) * is_first;
        memcpy(key, ptr + bgn_shift, key_size+1);
        // If it's the last file, the key would end with a ']'
        // that must be deleted
        if(key[key_size-1] == ']')
          key[key_size-1] = '\0';

        // Updates the hash table with the new information
        ht_update(ht, key, NULL, lc->client);

        is_first = 0;
        ptr = strtok_r(NULL, delim, &saveptr);
      }
    }
  } else {
    return copy_string(ERROR_MSG_CMD, strlen(ERROR_MSG_CMD),
                       answer, answer_size);
  }

  // Formats the answer
  char* a = "OK\n";
  printf("%s", a);
  return copy_string(a, strlen(a), answer, answer_size);
}

/**
  * Frees the client linked list
  */
void free_clients(){
  slist_client* lc;
  // Frees each item of the list
  while (!SLIST_EMPTY(&head_lc)){
    lc = SLIST_FIRST(&head_lc);
    SLIST_REMOVE_HEAD(&head_lc, next);
    free(lc->client);
    free(lc);
  }
}

/**
  * Frees all parser ressources
  */
void free_parser(){
  free_table(ht);
  free_clients();
}

/**
  * Print all the known files information
  */
void print_files(){
  print_table(ht);
}

/**
  * Parses a query
  * @param query       String to parse
  * @param ip          IP of the client
  * @param answer      Pointer to a string containing the answer
  * @param answer_size Size of the answer string
  * @return the new size of the answer string
  */
size_t process_query(const char* query, char* const ip,
                     char** const answer, size_t answer_size) {
  // If the query isn't valid, return an error message
  if(query == NULL || !strcmp(query, "")){
    answer_size = copy_string(ERROR_MSG_CMD, strlen(ERROR_MSG_CMD),
                              answer, answer_size);
    return answer_size;
  }

	char* str = malloc(sizeof(*str) * (strlen(query)+1));
  strcpy(str, query);

  // Pointer to the processing progress of a string
  char* saveptr;
	char *ptr = strtok_r(str, delim, &saveptr);

  // Determines query command and calls the appropriate processing function
  switch (id_command(ptr)) {
    case ANNOUNCE:
      answer_size = process_announce(saveptr, ip, answer, answer_size);
      break;
    case GETFILE:
      answer_size = process_getfile(saveptr, answer, answer_size);
      break;
    case LOOK:
      answer_size = process_look(saveptr, answer, answer_size);
      break;
    case UPDATE:
      answer_size = process_update(saveptr, ip, answer, answer_size);
      break;
    default:
      answer_size = copy_string(ERROR_MSG_CMD, strlen(ERROR_MSG_CMD),
                                answer, answer_size);
      break;
  }
  free(str);
  return answer_size;
}
