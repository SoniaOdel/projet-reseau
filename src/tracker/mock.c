#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#define USE_MOCK 0

typedef struct client
{
  //int id;
  char ip[30];
  int port;

}Client;


int main(void)
{
  int step = 1;

  int socketClient = socket(AF_INET, SOCK_STREAM, 0);
  struct sockaddr_in addrClient;
  addrClient.sin_addr.s_addr = inet_addr("127.0.0.1");
  addrClient.sin_family = AF_INET;
  addrClient.sin_port = htons(30000);

  if (connect(socketClient, (const struct sockaddr *)&addrClient, sizeof(addrClient)) < 0)
  {
    printf("Connection failed\n");
    exit(0);
  }
  printf("Connected to the server\n");

  //Client client;
  //client.ip = "127.0.0.1";
  char *join_msg = "< join 127.0.0.1";
  int msg_size = sizeof(*join_msg) * strlen(join_msg);
  char msg[200];

  while(1)
  {
    if (USE_MOCK == 1)
    {
      if (step == 0)
      {
        printf("%s\n", join_msg);
        if (send(socketClient, join_msg, msg_size, 0) >= 0)
        {
          printf("Join msg sended \n");
          sleep(1);
        }
      }
      else
        step = 1;
    }
    if (step == 1)
    {
      char *announce_msg = "announce listen 2222 seed [file_a.dat 2097152 1024 8905e92afeb80fc7722ec89eb0bf0966 file_b.dat 3145728 1536 330a57722ec8b0bf09669a2b35f88e9e] leech [1 2]";
      msg_size = sizeof(*announce_msg) * strlen(announce_msg);
      if (send(socketClient, announce_msg, msg_size, 0) >= 0)
      {
        printf("Announce msg  sended : %s\n",announce_msg);
        sleep(1);
      }
    }
    if (step == 2)
    {
      char* look_str = "look [filename=\"file_a.dat\" filesize>\"1048576\"]";
      msg_size = sizeof(*look_str) * strlen(look_str);
      if (send(socketClient, look_str, msg_size, 0) >= 0)
      {
        printf("Look msg sended : %s \n", look_str);
        sleep(1);
      }
    }
    if (step == 3)
    {
      char* getfile_str = "getfile 8905e92afeb80fc7722ec89eb0bf0966";
      msg_size = sizeof(*getfile_str) * strlen(getfile_str);
      if (send(socketClient, getfile_str, msg_size,0) >= 0)
      {
        printf("Getfile_str sended : %s \n", getfile_str);
        sleep(3);
        step++;
      }
    }
    memset(msg,0,200);
    while (recv(socketClient, msg, sizeof(msg)+1, 0) < 0 && step >= 1)
    {
      printf("En attente d'une réponse \n");
      sleep(1);
    }
    printf("Msg reçu : %s \n", msg);

    step++;


  }


  return 0;
}
