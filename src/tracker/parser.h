#ifndef PARSER_H
#define PARSER_H

#include <sys/queue.h>

/* Maximum length of a filename */
#define MAX_LEN_FILENAME 256
/* Returned message when encountering a parser error */
#define ERROR_MSG_CMD "Unknown command.\n"

enum command {ANNOUNCE, LISTEN, SEED, LEECH, UPDATE, GETFILE, LOOK};
enum comparator {GT, LT, EQ, NEQ, GEQ, LEQ};
enum field {NAME, SIZE, P_SIZE, KEY};

/* Array of characters separating commands or parameters from one another */
static const char delim[] = " ";
/* Array of comparison characters */
static const char cmp_chars[] = "<>!=";
/* Empty list of files */
static const char empty[] = "[]";

/* Ensures uniqueness of client id */
extern unsigned int id;
/* Hashtable used to store known files information */
struct hash_table* ht;

/* Linked list of known clients */
typedef struct slist_client {
    struct client* client;
    SLIST_ENTRY(slist_client) next;
} slist_client;

/* Head of the client linked list */
SLIST_HEAD(, slist_client) head_lc;

/* ***************************** */
/*           FUNCTIONS           */
/* ***************************** */

size_t process_query(const char*, char* const, char** const, size_t);
void print_files();
void init_parser() __attribute__((constructor));
void free_parser() __attribute__((destructor));

/* ***************************** */
/*         END FUNCTIONS         */
/* ***************************** */

#endif // PARSER_H
