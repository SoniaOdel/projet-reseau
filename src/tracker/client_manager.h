#ifndef CLIENT_MANAGER_H
#define CLIENT_MANAGER_H

/* Define a client (pair) */
typedef struct client {
    int id;
    char* ip;
    int port;
} client;

/* Define a linked list of clients */
typedef struct linkedlist_client linkedlist_client;
struct linkedlist_client {
    client* client;
    linkedlist_client* next;
};

/* ***************************** */
/*           FUNCTIONS           */
/* ***************************** */

linkedlist_client* allocate_list_client ();
linkedlist_client* linkedlist_client_insert(linkedlist_client* list,
  client* const client);
linkedlist_client* linkedlist_client_update(linkedlist_client* list,
  client* const client);
client* linkedlist_client_remove(linkedlist_client* list);
void free_linkedlist_client(linkedlist_client* list);

/* ***************************** */
/*         END FUNCTIONS         */
/* ***************************** */

#endif // CLIENT_MANAGER_H
