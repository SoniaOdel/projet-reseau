#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include <stddef.h>

/* Default size used for heap memory allocation */
#define DEFAULT_MALLOC_SIZE 40

/* ***************************** */
/*           FUNCTIONS           */
/* ***************************** */

size_t change_alloc_size_str(char** const s, const size_t s_size,
  const size_t new_size);
size_t double_alloc_size_str(char** const s, const size_t s_size);
size_t double_alloc_size(char*** s, const size_t s_size);
size_t copy_string(const char* const src, const size_t src_size,
  char** const dest, size_t dest_size);
size_t check_sprintf_size(char** const dest, size_t dest_size,
  const size_t space_needed);

  /* ***************************** */
  /*         END FUNCTIONS         */
  /* ***************************** */

#endif // MEMORY_MANAGER_H
