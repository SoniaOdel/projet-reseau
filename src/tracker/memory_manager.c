#include "memory_manager.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

/**
  * Modifies a heap memory allocation size of a string
  * @param s        Pointer to the allocated string
  * @param s_size   Current size of the allocation
  * @param new_size Desired new size of the allocation
  * @return the new size of the allocation if successful, the old size otherwise
  */
size_t change_alloc_size_str(char** const s, const size_t s_size,
                             const size_t new_size){
  if(*s){
    *s = realloc(*s, new_size * sizeof(**s));
    return new_size;
  }
  return s_size;
}

/**
  * Multiplies a heap memory allocation size of a string by two
  * @param s      Pointer to the allocated string
  * @param s_size Current size of the allocation
  * @return the new size of the allocation if successful, the old size otherwise
  */
size_t double_alloc_size_str(char** const s, const size_t s_size){
  return change_alloc_size_str(s, s_size, s_size*2);
}

/**
  * Multiplies a heap memory allocation size by two
  * @param s      Pointer to the allocated array of strings
  * @param s_size Current size of the allocation
  * @return the new size of the allocation if successful, the old size otherwise
  */
size_t double_alloc_size(char*** s, const size_t s_size){
  if(*s){
    *s = realloc(*s, s_size * 2 * sizeof(**s));
    return s_size * 2;
  }
  return s_size;
}

/**
  * Copies a string after ensuring the allocated destination size is sufficient
  * @param src       String to copy
  * @param src_size  Size of the string to copy
  * @param dest      Pointer to the allocated memory where to copy the string
  * @param dest_size Size of the allocated memory
  * @return the new size of the allocation if successful, the old size otherwise
  */
size_t copy_string(const char* const src, const size_t src_size,
                   char** const dest, size_t dest_size){
  while(src_size > dest_size)
    dest_size = double_alloc_size_str(dest, dest_size);
  strcpy(*dest, src);
  return dest_size;
}

/**
  * Ensures the size of an allocation is sufficient
  * @param dest         Pointer to the alloc. memory whose size must be checked
  * @param dest_size    Size of the allocated memory
  * @param space_needed Size needed for this allocation
  * @return the new size of the allocation if successful, the old size otherwise
  */
size_t check_sprintf_size(char** const dest, size_t dest_size,
                          const size_t space_needed){
  if(space_needed > dest_size)
    dest_size = change_alloc_size_str(dest, dest_size, space_needed);
  return dest_size;
}
