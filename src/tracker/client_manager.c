#include "client_manager.h"
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
/**
  * Allocates memory on the heap for a linkedlist_client
  * @return a pointer to the new linkedlist_client
  */
linkedlist_client* allocate_list_client () {
    // Allocates memory for a Linkedlist_client pointer
    linkedlist_client* list = malloc(sizeof(*list));
    return list;
}

/**
  * Inserts a client at the end of a linkedlist_client
  * @param list   Linked list to insert into
  * @param client Client to insert
  * @return a pointer to the linked list
  */
linkedlist_client* linkedlist_client_insert(linkedlist_client* list,
                                            client* const client) {
    /* The list is empty */
    if (!list) {
        linkedlist_client* head = allocate_list_client();
        head->client = client;
        head->next = NULL;
        list = head;
        return list;
    }

    /* The list only contains one element */
    else if (list->next == NULL) {
        linkedlist_client* node = allocate_list_client();
        node->client = client;
        node->next = NULL;
        list->next = node;
        return list;
    }

    /* Browse through the list to the tail */
    linkedlist_client* temp = list;
    while (temp->next) {
        temp = temp->next;
    }

    /* Append the new element */
    linkedlist_client* node = allocate_list_client();
    node->client = client;
    node->next = NULL;
    temp->next = node;

    return list;
}

/**
  * Inserts a client in a linkedlist_client is it isn't already there
  * @param list   Linked list to insert into
  * @param client Client to insert
  * @return a pointer to the linked list
  */
linkedlist_client* linkedlist_client_update(linkedlist_client* list,
                                            client* const client) {
    /* The list is empty */
    if (!list) {
        linkedlist_client* head = allocate_list_client();
        head->client = client;
        head->next = NULL;
        list = head;
        return list;
    }

    /* The list only contains one element */
    else if (list->next == NULL) {
        if(list->client == client)
          return list;

        linkedlist_client* node = allocate_list_client();
        node->client = client;
        node->next = NULL;
        list->next = node;

        return list;
    }

    /* Browse through the list to the tail */
    linkedlist_client* temp = list;
    while (temp->next) {
        if(temp->client == client)
          return list;
        temp = temp->next;
    }
    if(temp->client == client)
      return list;

    /* Append the new element */
    linkedlist_client* node = allocate_list_client();
    node->client = client;
    node->next = NULL;
    temp->next = node;

    return list;
}

/**
  * Removes the head from the linkedlist_client
  * @param list Linked list to remove from
  * @return the item of the popped element
  */
client* linkedlist_client_remove(linkedlist_client* list) {
    if (!list)
        return NULL;
    if (!list->next)
        return NULL;
    linkedlist_client* node = list->next;
    linkedlist_client* temp = list;
    temp->next = NULL;
    list = node;
    client* c = NULL;
    memcpy(temp->client, c, sizeof(*c));
    free(temp->client->ip);
    free(temp->client);
    free(temp);
    return c;
}

/**
  * Frees all items of a linkedlist_client
  * @param list Linked list to free
  */
void free_linkedlist_client(linkedlist_client* list) {
    linkedlist_client* temp = list;
    if (!list)
        return;
    while (list) {
        temp = list;
        list = list->next;
        free(temp);
    }
}
