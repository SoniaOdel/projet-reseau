#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include "client_manager.h"

/* Size of the hash table */
#define CAPACITY 8000
/* Prime numbers used in the hash function to avoid too much collisions */
#define PRIME_1 7
#define PRIME_2 31

/* Define a hash table item, i.e. a file */
typedef struct file_p2p ht_item;
typedef struct file_p2p {
  char* name;
  size_t size; // in byte
  size_t pieces_size; // in byte
  char* key;

  linkedlist_client* seeders;
  linkedlist_client* leechers;
} file_p2p;

/* Define a linked list of hash table items (for duplicates) */
typedef struct linkedlist_item linkedlist_item;
struct linkedlist_item {
    ht_item* item;
    linkedlist_item* next;
};

/* Define the hash table */
typedef struct hash_table hash_table;
struct hash_table {
    /* Array of pointers to items */
    ht_item** items;
    /* Array of pointers to linkedlist for duplicates */
    linkedlist_item** overflow_buckets;
    size_t size;
    size_t count;
};

/* ***************************** */
/*           FUNCTIONS           */
/* ***************************** */

hash_table* create_table(const size_t size);
void free_table(hash_table* const table);
void ht_insert(hash_table* const table, const char* const key,
  const char* const name, const size_t size, const size_t pieces_size,
  client* const seeder, client* const leecher);
void ht_update(hash_table* const table, const char* const key,
  client* const seeder, client* const leecher);
ht_item* ht_search(const hash_table* const table, const char* const key);
void ht_delete(hash_table* const table, const char* const key);
void print_table(const hash_table* const table);
void print_search(const hash_table* const table, const char* const key);
size_t ht_get_count(const hash_table* const table);
void ht_retrieve_all(const hash_table* const table,
  char** const files, const size_t size);

/* ***************************** */
/*         END FUNCTIONS         */
/* ***************************** */

#endif // HASH_TABLE_H
