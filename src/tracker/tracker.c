#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <parser.h>
#include <assert.h>
#include "thpool.h"
#include "memory_manager.h"

#define USE_MOCK 0

struct client_args {
  int *socket;
  char *ip;
};


void function(void *args)
{
  // Récupération des arguments :
  struct client_args *arguments = (struct client_args *)args;
  int socket = *(int *)(arguments->socket);
  char *ip = arguments->ip;

  size_t answer_size = DEFAULT_MALLOC_SIZE;
  char* answer = malloc (sizeof(*answer) * answer_size);

  printf("-------- New client connected ! --------\n");

  // Partie utile pour des tests (avec le mock)
  if (USE_MOCK == 1)
  {
  char msg_mock[16];
    while (recv(socket, msg_mock, sizeof(msg_mock)+1, 0) < 0)
    {
      printf("On attend un message \n");
    }

    printf("Message reçu : %s\n",msg_mock);


    char msgj[10] = "Join reçu";
    send(socket, msgj, 10, 0);
  }

  // Partie normale
  char msg[500];
  while(1)
  {
    memset(msg,0,500);
    printf(" -------- Waiting request -------- \n");
    if (recv(socket,msg, sizeof(msg)+1, 0) < 0)
      printf("Error in reception of msg\n");
    else
    {
      printf("Message Received : %s \n \n",msg);
      // On lance le parseur
      answer_size = process_query(msg, ip, &answer, answer_size);
      assert(strlen(answer) <= answer_size);
      // On renvoie la réponse au client
      //printf("Answer : %s \n", answer);
      send(socket, answer, strlen(answer), 0);
    }
  }
  free(answer);
}



int main(void)
{
  // Initialisation : TCP IPv4
  struct sockaddr_in addrServer;
  int socketServer = socket(AF_INET, SOCK_STREAM, 0);
  addrServer.sin_addr.s_addr = inet_addr("127.0.0.1");
  addrServer.sin_family = AF_INET;
  addrServer.sin_port = htons(30000);

  /*
    Nommage local :
     On associe l'adresse addrServer au socket socketServer
  */
  bind(socketServer, (const struct sockaddr *)&addrServer, sizeof(addrServer));

  // On fait écouter notre socket
  listen(socketServer,5);
  printf("Tracker launched, listening on the socket\n");

  // Création de la threadpool permettant de stocker les clients
  threadpool thpool = thpool_init(10);

  int *arg = malloc(sizeof(int));
  for (int i = 0; i < 10; i++)
  {
    struct sockaddr_in addrClient;
    // Récupération de la taille de l'adresse du client
    socklen_t csize = sizeof(addrClient);
    // On cré un nouveau socket pour la connexion acceptée
    int socketClient = accept(socketServer, (struct sockaddr *)&addrClient, &csize);

    char *ip_address = inet_ntoa(addrClient.sin_addr);

    *arg = socketClient;

    struct client_args args;
    args.socket = arg;
    args.ip = ip_address;

    //pthread_create(&threads[i], NULL, function, (void*)&args);
    thpool_add_work(thpool, function, (void*)&args);
  }
/*
  for(int i = 0; i < 3; i++)
  {
    pthread_join(threads[i],NULL);
  }
*/
  close(socketServer);

  printf("close\n");
  free(arg);
  return 0;
}
