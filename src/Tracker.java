import java.net.*;
import java.io.*;
import java.util.*;
import javax.crypto.*;
import java.security.*;
import java.math.BigInteger;


public class Tracker{
    static final int port = 20000;
    public static final String addressIP = "127.0.0.10";
    static final int nbClients = 3;

    public static void main(String[] args) throws Exception {
	ServerSocket s = new ServerSocket(port);
	System.out.println("Tracker lancé");
	for (int i = 0; i < nbClients; i++) {
	    Socket soc = s.accept();
	    BufferedInputStream bis = new BufferedInputStream(soc.getInputStream());
	    BufferedOutputStream bos = new BufferedOutputStream(soc.getOutputStream());
	    
	    //reception annonce
	    StringBuffer sb = new StringBuffer();
	    int b;
	    while ((b=bis.read()) == -1) {}
	    sb.append((char)b);
	    while ((b=bis.read()) != -1) {
		if(b == '\n') break; 
		sb.append((char)b);
	    }
	    String res = sb.toString();
	    System.out.println("> " + res);

	    //reponse ok
	    String mts = "ok\r\n";
	    byte[] mtss = mts.getBytes();
	    bos.write(mtss,0,mtss.length);
	    bos.flush();
	    System.out.println("< " + mts);
	    
	    //reception look
	    sb = new StringBuffer();
	    int bl;
	    while ((bl=bis.read()) == -1) {}
	    sb.append((char)bl);
	    while ((bl=bis.read()) != -1) {
		if(bl == '\n') break; 
		sb.append((char)bl);
	    }
	    res = sb.toString();
	    System.out.println("> " + res);
	    
	    //reponse look
	    String mtsl = "list [Dali.txt 5028 1024 887887776f2b767805d9dee20fb81898]\r\n";
	    byte[] mtssl = mtsl.getBytes();
	    bos.write(mtssl,0,mtssl.length);
	    bos.flush();
	    System.out.println("< " + mtsl);
	    
	    
	    //reception getfile
	    sb = new StringBuffer();
	    int bg;
	    while ((bg=bis.read()) == -1) {}
	    sb.append((char)bg);
	    while ((bg=bis.read()) != -1) {
		if(bg == '\n') break; 
		sb.append((char)bg);
	    }
	    res = sb.toString();
	    System.out.println("> " + res);
	    
	    //reponse getfile
	    String mtsg = "peers 887887776f2b767805d9dee20fb81898 [1.1.1.2:2222 1.1.1.3:3333]\r\n";
	    byte[] mtssg = mtsg.getBytes();
	    bos.write(mtssg,0,mtssg.length);
	    bos.flush();
	    System.out.println("< " + mtsg);


	    //reception update
	    sb = new StringBuffer();
	    int bu;
	    while ((bu=bis.read()) == -1) {}
	    sb.append((char)bu);
	    while ((bu=bis.read()) != -1) {
		if(bu == '\n') break; 
		sb.append((char)bu);
	    }
	    res = sb.toString();
	    System.out.println("< " + res);

	    //reponse ok
	    String mtsu = "ok\r\n";
	    byte[] mtssu = mtsu.getBytes();
	    bos.write(mtssu,0,mtssu.length);
	    bos.flush();
	    System.out.println("> " + mtsu);
	    
	    bis.close();
	    bos.close();
	    soc.close();
	}
    }
}
