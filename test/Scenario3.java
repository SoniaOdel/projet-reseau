import java.net.*;
import java.io.*;
import java.util.*;
import javax.crypto.*;
import java.security.*;
import java.math.BigInteger;
import java.util.Arrays;
import java.lang.*;


public class Scenario3 {

    public static void main(String[] args) {
	
	try {
	    System.out.println("Enter the peer address of the peer");
	    BufferedReader hey = new BufferedReader(new InputStreamReader(System.in));
	    String ipad = hey.readLine();
	    System.out.println("Enter the peer id of the peer");
	    int id = Integer.parseInt(hey.readLine());
	    Client me = new Client(ipad,id);
	    Timer timer = new Timer();
	    timer.schedule(me.new Updater(),5000,30000);
	    if (id == 1001) {
		Hashtable<String,Integer> look = me.look("look [filename=\"Dali.txt\"]");
		me.update();
		FileManager fm = new FileManager(me.getId(), "Dali.txt", me.pieceLen);
		String key = look.keySet().toArray()[0].toString();
		Hashtable<String,Integer> h = new Hashtable<String,Integer>();
		h = me.getfile(key);
		Iterator<String> tmp = h.keySet().iterator();
		String iterator;
		while(tmp.hasNext()){
		    iterator = tmp.next();
		    System.out.println("IP " + iterator + " port " + h.get(iterator));
		}
		me.interested(h,key, look.get(key));
	    } else if (id == 1002 || id == 1003) {
	    } 
	} catch (IOException e) {
	    System.out.println("Pb test io");
	} catch (NoSuchAlgorithmException nsae) {
	    throw new IllegalStateException(nsae);
	} catch (Exception e) {
	    System.out.println("Pb test ex");
	}
    }
}
