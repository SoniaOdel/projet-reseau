#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "parser.h"

int main() {
  char* answer = malloc(sizeof(*answer) * 1000);

  /*char* announce_str = "announce listen 4444 seed [file_a.dat 2097152 1024 8905e92afeb80fc7722ec89eb0bf0966 file_b.dat 3145728 1536 330a57722ec8b0bf09669a2b35f88e9e] leech [8905e92afeb80fc7722ec89eb0bf0966]";
  char* look_str = "look [filename=\"file_a.dat\" filesize>\"1048576\"]";
  char* getfile_str = "getfile 8905e92afeb80fc7722ec89eb0bf0966";
  char* announce_empty_str = "announce listen 5555 seed [] leech []";
  char* update_str = "update seed [8905e92afeb80fc7722ec89eb0bf0966 330a57722ec8b0bf09669a2b35f88e9e] leech [8905e92afeb80fc7722ec89eb0bf0966]";

  printf("\n< %s\n", announce_str);
  process_query(announce_str, "ip4", &answer, strlen(announce_str));
  printf("> %s\n", answer);
  print_files();

  printf("\n< %s\n", look_str);
  process_query(look_str, "ip4", &answer, strlen(look_str));
  printf("> %s\n", answer);
  printf("\n< %s\n", getfile_str);
  process_query(getfile_str, "ip4", &answer, strlen(getfile_str));
  printf("> %s\n", answer);

  printf("\n< %s\n", announce_empty_str);
  process_query(announce_empty_str, "ip5", &answer, strlen(announce_empty_str));
  printf("> %s\n", answer);
  print_files();
  printf("\n< %s\n", update_str);
  process_query(update_str, "ip5", &answer, strlen(update_str));
  printf("> %s\n", answer);
  print_files();*/

  /* Real scenario */
  char* announce1 = "announce listen 2222 seed [DeVinci.txt 2138 1024 c314266eb83b4c46dcd97ee3cf2711b1] leech [98781aedf02262a399557648ff32df45]";
  char* announce2 = "announce listen 3333 seed [Dali.txt 2158 1024 98781aedf02262a399557648ff32df45 VanGogh.txt 3507 1024 0286d1ee670420c35ad42241599951d6] leech []";
  char* getfile1 = "getfile 0286d1ee670420c35ad42241599951d6";
  char* look1 = "look [filename=\"DeVinci.txt\"]";
  char* look2 = "look [filename=\"Unknown.txt\"]";
  char* look3 = "look [filename!=\"DeVinci.txt\" filesize<3000]";
  char* look4 = "look [piecessize>=1024]";
  char* update1 = "update seed [c314266eb83b4c46dcd97ee3cf2711b1 0286d1ee670420c35ad42241599951d6] leech [98781aedf02262a399557648ff32df45]";
  char* getfile2 = "getfile 0286d1ee670420c35ad42241599951d6";
  char* getfile3 = "getfile 00000000000000000000000000000001";

  printf("\n< %s\n", announce1);
  process_query(announce1, "ip2", &answer, strlen(announce1));
  printf("> %s\n", answer);
  print_files();
  printf("\n< %s\n", announce2);
  process_query(announce2, "ip3", &answer, strlen(announce2));
  printf("> %s\n", answer);
  print_files();

  printf("\n< %s\n", getfile1);
  process_query(getfile1, "ip2", &answer, strlen(getfile1));
  printf("> %s\n", answer);
  assert(!strcmp(answer, "peers 0286d1ee670420c35ad42241599951d6 [ip3:3333]\n"));

  printf("\n< %s\n", look1);
  process_query(look1, "ip2", &answer, strlen(look1));
  printf("> %s\n", answer);
  assert(!strcmp(answer, "list [DeVinci.txt 2138 1024 c314266eb83b4c46dcd97ee3cf2711b1]\n"));
  printf("\n< %s\n", look2);
  process_query(look2, "ip2", &answer, strlen(look2));
  printf("> %s\n", answer);
  assert(!strcmp(answer, "list []\n"));
  printf("\n< %s\n", look3);
  process_query(look3, "ip2", &answer, strlen(look3));
  printf("> %s\n", answer);
  assert(!strcmp(answer, "list [Dali.txt 2158 1024 98781aedf02262a399557648ff32df45]\n"));
  printf("\n< %s\n", look4);
  process_query(look4, "ip2", &answer, strlen(look4));
  printf("> %s\n", answer);
  assert(!strcmp(answer, "list [VanGogh.txt 3507 1024 0286d1ee670420c35ad42241599951d6 Dali.txt 2158 1024 98781aedf02262a399557648ff32df45 DeVinci.txt 2138 1024 c314266eb83b4c46dcd97ee3cf2711b1]\n"));

  printf("\n< %s\n", update1);
  process_query(update1, "ip2", &answer, strlen(update1));
  printf("> %s\n", answer);
  print_files();

  printf("\n< %s\n", getfile2);
  process_query(getfile2, "ip2", &answer, strlen(getfile2));
  printf("> %s\n", answer);
  assert(!strcmp(answer, "peers 0286d1ee670420c35ad42241599951d6 [ip3:3333 ip2:2222]\n"));

  printf("\n< %s\n", getfile3);
  process_query(getfile3, "ip2", &answer, strlen(getfile3));
  printf("> %s\n", answer);
  assert(!strcmp(answer, "peers 00000000000000000000000000000001 []\n"));

  free(answer);

  return 0;
}
