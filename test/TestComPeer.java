import java.net.*;
import java.io.*;
import java.util.*;
import javax.crypto.*;
import java.security.*;
import org.ini4j.Ini;
import java.math.BigInteger;
import java.util.Arrays;
import java.lang.*;

public class TestComPeer {

    public static void main(String[] args) {
	try {
	    //instanciation du client clients
	    System.out.println("Enter the peer address of the peer");
	    BufferedReader bur = new BufferedReader(new InputStreamReader(System.in));
	    String ipad = bur.readLine();
	    System.out.println("Enter the peer id of the peer");
	    int id = Integer.parseInt(bur.readLine());
	    Client c = new Client(ipad,id);

	    if (id == 1002){}

	    else if (id == 1001){	
		
		//récupération des données pour effectuer l'action interested
		FileManager fm = new FileManager(1002, "Dali.txt", c.pieceLen);
		String key = fm.getKey();
		long totLen = fm.getFileLength();
		Hashtable<String,Integer> conn = new Hashtable<String,Integer>();
		conn.put("127.0.0.4",3333);

		//mise en place de la structure d'accueil
		LocalFile file = new LocalFile(id, "Dali.txt", c.pieceLen);
		file.setKey(key);
		file.setLength((int)totLen);
		c.leech.put(file.getKey(), new BitSet());
		c.filenames.put(file.getKey(), "Dali.txt");
		
		//fonction interested
		Hashtable<String,BitSet> interested = c.interested(conn,key,(int)totLen);
	    }
	} catch (NoSuchAlgorithmException nsae) {
	    throw new IllegalStateException(nsae);
	} catch (Exception e) {
	    System.out.println("Erreur Test Com 2 Peer Only");
	}
    }
}
