import java.util.BitSet;
import java.util.*;

public class TestFalseTracker{

    public static void main(String[] args)
    {
	Client c1 = new Client("127.0.0.1",1001);
	try {
	    //recuperation cle + taille totale
	    Hashtable<String,Integer> look = c1.look("look [filename=\"Dali.txt\"]\n");

	    //Retour des données sur tous les résultats présents dans look
	    Iterator<String> kl = look.keySet().iterator();
	    String iterkl;
	    String key = "";
	    int totalLength;
	    while(kl.hasNext()) {
		iterkl = kl.next();
		System.out.println("Key : " + iterkl + " Total Lentgh : " + look.get(iterkl));
		key = iterkl;
		totalLength = look.get(iterkl);
	    }

	    //fonction getfile
	    Hashtable<String,Integer> getfile = c1.getfile(key);

	    //retour des données dans list
	    Iterator<String> gf = getfile.keySet().iterator();
	    String itergf;
	    while(gf.hasNext()){
		itergf = gf.next();
		System.out.println("IP " + itergf + " port " + getfile.get(itergf));
		}

	    //fonction update
	    c1.update();
	    c1.endConTrack();
	} catch (Exception e) {
	    System.out.println("Erreur Test False Tracker");
	}
    }
}
