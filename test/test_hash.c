#include <stdio.h>
#include <stdlib.h>
#include "hash_table.h"

int main() {
    struct hash_table* ht = create_table(CAPACITY);
    client* c1 = malloc(sizeof(*c1));
    c1->id = 1; c1->ip = "1"; c1->port = 1;
    client* c2 = malloc(sizeof(*c2));
    c2->id = 2; c2->ip = "2"; c2->port = 2;
    client* c3 = malloc(sizeof(*c3));
    c3->id = 3; c3->ip = "3"; c3->port = 3;
    client* c4 = malloc(sizeof(*c4));
    c4->id = 4; c4->ip = "4"; c4->port = 4;

    printf("\tINSERT\n");
    ht_insert(ht, "key1", "Name1", 1, 1, c1, NULL);
    ht_insert(ht, "key1", "Name1", 1, 1, c2, NULL);
    ht_insert(ht, "key2", "Name2", 2, 2, c1, NULL);
    ht_insert(ht, "key2", "Name2", 2, 2, NULL, c2);
    ht_insert(ht, "key3", "Name3", 3, 3, NULL, NULL);
    ht_insert(ht, "key4", "Name4", 4, 4, c3, NULL);
    ht_insert(ht, "key5", NULL, 0, 0, NULL, c3);

    printf("\tSEARCH\n");
    print_search(ht, "key1");
    print_search(ht, "key2");
    print_search(ht, "key3");
    print_search(ht, "key4");
    print_search(ht, "key5");
    print_search(ht, "RAND");
    printf("\tTABLE");
    print_table(ht);

    printf("\tDELETE\n");
    ht_delete(ht, "key1");
    ht_delete(ht, "key4");
    ht_delete(ht, "key5");
    printf("\tTABLE");
    print_table(ht);

    printf("\tUPDATE\n");
    ht_update(ht, "key3", NULL, c1);
    ht_update(ht, "key3", c2, NULL);
    ht_update(ht, "key3", c3, NULL);
    ht_update(ht, "key3", c4, NULL);
    ht_update(ht, "key3", c2, NULL);
    ht_update(ht, "key3", c3, NULL);
    ht_update(ht, "key3", c4, NULL);
    printf("\tTABLE");
    print_table(ht);

    free_table(ht);
    free(c1);
    free(c2);
    free(c3);
    free(c4);

    return 0;
}
