//package peer;
import java.util.*;
import java.net.*;
import java.io.*;
import javax.crypto.*;
import java.security.*;
//import org.ini4j.Ini;
import java.math.BigInteger;
import java.util.Arrays;
import java.lang.*;


public class Scenario1 {

	public static void main(String[] args) {
		try {
			System.out.println("Enter the peer address of the peer");
			BufferedReader parser = new BufferedReader(new InputStreamReader(System.in));
			String ipad = parser.readLine();
			System.out.println("Enter the peer id of the peer");
			int id = Integer.parseInt(parser.readLine());
			Client me = new Client(ipad, id);
			Timer timer = new Timer();
			timer.schedule(me.new Updater(), 30000, 20000);
			Hashtable<Integer,String> h = new Hashtable<Integer,String>();
			String key;
			Hashtable<String,Integer> look;
			switch (id) {
				case 1001:
					Thread.sleep(60000);
					look = me.look("look [filename=\"Dali.txt\"]");
					key = look.keySet().toArray()[0].toString();
					h = me.getfile(key);
					me.interested(h, key,look.get(key));
					break;

				case 1003:
					Thread.sleep(60000);
					look = me.look("look [filename=\"Munch.txt\"]");
					key = look.keySet().toArray()[0].toString();
					h = me.getfile(key);
					me.interested(h, key,look.get(key));
					break;
			}
		} catch (Exception e) {
		}
	}
}
