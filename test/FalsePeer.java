//package peer;
import java.net.*;
import java.io.*;
import java.lang.*;
import java.util.BitSet;

public class FalsePeer{
    private int port;
    private int id;
    private BitSet bufferMap;
    private Server server;
    private Socket socket;
    
    public FalsePeer(int id, String adressIp, int port,int maxHandlers, int maxWaitingConn,BitSet bufferMap){
	this.id = id;
	this.port = port;
	this.bufferMap = bufferMap;
	this.server = new Server(adressIp,port,maxHandlers,maxWaitingConn,this);
	//this.socket = new MySocket(this);
	System.out.println("Server lauched on port " + this.port );

	server.start();
    }

    public void connect(String adressIP, int port){
	try{
	    InetAddress ip = InetAddress.getByName(adressIP);
	    Socket s = new Socket(ip, port);
	    this.socket = s;
	    // this.socket.update(s);
	} 
	catch(Exception e){
	    System.out.println("Exception in connect");
	}
	
    }/*
       public void close(){
       try{
       mySocket.close();
       }
       catch(Exception e){
       }
       }
     */

    public int getPort(){
	return this.port;
    }

    public int getId(){
	return this.id;
    }
    
    public byte[] pieceFromKey(String key, int piece, int len){
	String s =  "juste un test";
	byte[] b = s.getBytes();
	return b;
    }
    
    public Object[] buffermapFromKey(String key){
	return new Object[]{this.bufferMap,4};
    }

    public int[] compareBufferMap(BitSet b){
    	int len = b.size();
	int[] ret = new int[len];
	int pos = 0;
	int i=-1;
	for(i=0; i<len; i++){
	    if(b.get(i) && !this.bufferMap.get(i)){
		ret[pos] = i;
		this.bufferMap.set(i);//a faire dans le traitement de la repon
	    }
	    else
		ret[pos] = -1;
	    pos++;
	}
	return ret;
    }

    //fonction de telechargement pour le test. A voir comment sera implémenter les
    //methodes de connection pair/server au niveau de la partie client du pair
    public void download(String fileKey){
	try{
	    BufferedInputStream reader = new BufferedInputStream(this.socket.getInputStream());
	    BufferedOutputStream writer = new BufferedOutputStream(this.socket.getOutputStream());
	    String msg = "interested " + fileKey+'\n';
	    byte[] output = msg.getBytes();
	    /*byte[] len = new byte[1];
	      len[0] = (byte)output.length;
	      writer.write(len,0,1);
	      writer.flush();*/
	    writer.write(output,0,output.length);
	    writer.flush();
	    Requete requete = new Requete();
	    
	    while(true){
	    	msg = new String();
	    	int oneChar;
	
		/*reader.read(len,0,1);
		  byte[] input = new byte[(int)len[0]];
		  reader.read(input,0,(int)len[0]);
		  msg = new String(input);
		*/

		while ((oneChar = reader.read()) != -1 && (char)oneChar != '\n') {
		    msg += (char) oneChar;
		    //System.out.println(msg);
		}
	
		requete.integrity(msg);
		System.out.println(msg);
		System.out.println("To "+ this.id + " : < " + msg);
		switch(requete.keyWord){
		case "have":
		    msg = new String();
		    msg = "getpieces " + requete.fileKey+" [";
		    //
		    int[] index = this.compareBufferMap(requete.bufferMap);
				  
		    for(int i=0; i<index.length; i++){
			if(index[i] != -1)
			    msg = msg+index[i]+" ";
		    }
		    // }
		    msg = msg.trim();
		    msg = msg+"]\n";
		    output = msg.getBytes();
		    /* len[0] = (byte)output.length;
		       writer.write(len,0,1);
		       writer.flush();*/
		    writer.write(output,0,output.length);
		    writer.flush();
		    
		    break;
		case "data":
		    msg = "close\n";
		    output = msg.getBytes();
		    /*len[0] = (byte)output.length;
		      writer.write(len,0,1);
		      writer.flush();*/
		    writer.write(output,0,output.length);
		    writer.flush();
		    this.socket.close();
		    break;
		    /*case default :
		      System.out.println("ras");
		      break;
		    */
		}
		
	    }
	}
    
	catch(Exception e){
	}
    }

    public String printBufferMap(){
	String s = "[ ";
	for (int i=0; i<this.bufferMap.length();i++){
	    if(this.bufferMap.get(i))
		s += "1 ";
	    else
		s += "0 ";
	}
	s += "]";
	return s;
    }
    
    public void ToString(){
	System.out.println( "<Peer id : " + this.id + ", port: " + this.port + ", bufferMap: " + this.printBufferMap() + ">");
    }


    /*
      public void upload(String fileKey){
      try{
      socket.start(fileKey);
      }
      catch (Exception e){
      System.out.println("Exception in upload");
      }
      }
    */

}

    

