package peer;
import java.util.BitSet;

public class FalsePeerTest{

    public static void main(String[] args)
    {
       BitSet b1 = new BitSet(4);
       b1.set(3);
       BitSet  b2 = new BitSet(4);
       b2.set(0); b2.set(2); b2.set(3);
       BitSet  b3 = new BitSet(4);
       b3.set(2,3);
       BitSet  b4 = new BitSet(4);
       b1.set(0,3,false);
       FalsePeer p1 = new FalsePeer(1,"127.0.0.1",5000,4,4,b1);
       FalsePeer p2 = new FalsePeer(2,"127.0.0.1",6000,3,3,b2);
       FalsePeer p3 = new FalsePeer(3,"127.0.0.1",7000,6,6,b3);
       FalsePeer p4 = new FalsePeer(4,"127.0.0.1",8000,6,6,b4);
       p1.ToString();
       p2.ToString();
       p3.ToString();
       p4.ToString();
       p1.connect("127.0.0.1",6000);
       p3.connect("127.0.0.1",6000);
       p1.download("dfghjk");
       p1.ToString();
       p3.download("erty");
       p3.ToString();
       p4.connect("127.0.0.1",7000);
       p4.download("erty");
       p1.ToString();
       p2.ToString();
       p3.ToString();
       p4.ToString();
       
    }
}
