CC=gcc
CFLAGS_TRACKER=-g -O0 -W -Wall -pedantic
CFLAGS=-std=c99 -Wall
JC = javac
J = java -ea
SRC_PEER=./src/peer
SRC_PARSER=./src_parser
SRC_HASH=./src_hash
SRC_TRACKER=./src/tracker
SRC_TST=./src/tests
TST=./test
INSTALL_DIR=./install
BUILD_DIR=./build
BIN_DIR=bin

all: clean tracker mock manual_mock build_peer #tests_peer test

test: test_hash test_parser

#---------------------------------- C ------------------------------

test_parser: $(BUILD_DIR)/test_parser

$(BUILD_DIR)/test_parser: $(BUILD_DIR)/test_parser.o $(BUILD_DIR)/parser.o $(BUILD_DIR)/hash_table.o $(BUILD_DIR)/client_manager.o $(BUILD_DIR)/memory_manager.o
	$(CC) $(CFLAGS_TRACKER) -o $@ $^
	./$@

$(BUILD_DIR)/test_parser.o: $(TST)/test_parser.c
	$(CC) $(CFLAGS) -I$(SRC_TRACKER) -o $@ -c $<

test_hash: $(BUILD_DIR)/test_hash

$(BUILD_DIR)/test_hash: $(BUILD_DIR)/test_hash.o $(BUILD_DIR)/hash_table.o $(BUILD_DIR)/client_manager.o
	$(CC) $(CFLAGS_TRACKER) -o $@ $^ -lm
	./$@

$(BUILD_DIR)/test_hash.o: $(TST)/test_hash.c
	$(CC) $(CFLAGS_TRACKER) -I$(SRC_TRACKER) -o $@ -c $<

# -----------------------------------------------------------------

$(SRC_TRACKER)/hash_table.c: $(SRC_TRACKER)/hash_table.h

$(BUILD_DIR)/hash_table.o: $(SRC_TRACKER)/hash_table.c
	$(CC) $(CFLAGS_TRACKER) -c $< -o $@

$(SRC_TRACKER)/parser.c: $(SRC_TRACKER)/parser.h

$(BUILD_DIR)/parser.o: $(SRC_TRACKER)/parser.c
	$(CC) $(CFLAGS_TRACKER) -o $@ -c $<

$(SRC_TRACKER)/memory_manager.c: $(SRC_TRACKER)/memory_manager.h

$(BUILD_DIR)/memory_manager.o : $(SRC_TRACKER)/memory_manager.c
	$(CC) $(CFLAGS) -o $@ -c $<

$(SRC_TRACKER)/client_manager.c: $(SRC_TRACKER)/client_manager.h

$(BUILD_DIR)/client_manager.o : $(SRC_TRACKER)/client_manager.c
	$(CC) $(CFLAGS) -o $@ -c $<


tracker: $(BUILD_DIR)/tracker

$(BUILD_DIR)/tracker.c: $(SRC_TRACKER)/tracker.h $(SRC_TRACKER)/parser.h $(SRC_TRACKER)/hash_table.h

$(BUILD_DIR)/tracker.o: $(SRC_TRACKER)/tracker.c
		$(CC) $(CFLAGS) -I$(SRC_TRACKER) -c $< -o $@

$(SRC_TRACKER)/thpool.c: $(SRC_TRACKER)/thpool.h

$(BUILD_DIR)/thpool.o : $(SRC_TRACKER)/thpool.c
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILD_DIR)/tracker: $(BUILD_DIR)/tracker.o $(BUILD_DIR)/thpool.o $(BUILD_DIR)/parser.o $(BUILD_DIR)/hash_table.o $(BUILD_DIR)/memory_manager.o $(BUILD_DIR)/client_manager.o
	$(CC) $(CFLAGS_TRACKER) -o $@ $^ -lpthread

mock: $(BUILD_DIR)/mock

$(BUILD_DIR)/mock: $(SRC_TRACKER)/mock.c
	$(CC) $(CFLAGS) $< -o $@

manual_mock: $(BUILD_DIR)/manual_mock

$(BUILD_DIR)/manual_mock: $(SRC_TRACKER)/manual_mock.c
	$(CC) $(CFLAGS) $< -o $@

install: test tracker mock
	mkdir -p install
	cp $(BUILD_DIR)/mock $(INSTALL_DIR)/
	cp $(BUILD_DIR)/tracker $(INSTALL_DIR)/
	cp $(BUILD_DIR)/test_hash $(INSTALL_DIR)/
	cp $(BUILD_DIR)/test_parser $(INSTALL_DIR)/


client:

#-------------------------------------- JAVA --------------------------------

build_peer:
	$(JC) -d $(BUILD_DIR) $(SRC_PEER)/*.java
	$(JC) -d $(BUILD_DIR) -cp $(BUILD_DIR) $(TST)/Scenario1.java
	$(JC) -d $(BUILD_DIR) -cp $(BUILD_DIR) $(TST)/Scenario2.java
	cp -r org/ $(BUILD_DIR)
	rm -rf src/peer/peersfiles/peer1001/files/pieces/Dali.txt
	rm -rf src/peer/peersfiles/peer1003/files/pieces/Munch.txt

Scenario1:
	$(J) -cp $(BUILD_DIR) Scenario1

Scenario2:
	$(J) -cp $(BUILD_DIR) Scenario2

TrackerJava:
	rm -rf src/peers/peer1001/files/pieces/Dali.txt
	$(JC) -d $(BUILD_DIR) $(SRC)/*.java
	$(JC) -d $(BUILD_DIR) -cp $(BUILD_DIR) $(SRC)/Tracker.java
	$(J) -cp $(BUILD_DIR) Tracker

TestFalseTracker:
	$(JC) -d $(BUILD_DIR) -cp $(BUILD_DIR) $(TST)/TestFalseTracker.java
	$(J) -cp $(BUILD_DIR) TestFalseTracker

TestComPeer:
	$(JC) -d $(BUILD_DIR) $(SRC)/*.java
	cp -r org/ $(BUILD_DIR)
	$(JC) -d $(BUILD_DIR) -cp $(BUILD_DIR) $(TST)/TestComPeer.java
	$(J) -cp $(BUILD_DIR) TestComPeer


clean:
	rm -rf $(BUILD_DIR)/*
	rm -rf $(BIN_DIR)/*
